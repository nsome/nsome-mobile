import { Navigation } from 'react-native-navigation';
import EStyleSheet from 'react-native-extended-stylesheet';

import LaunchScreen from './LaunchScreen';
import LoginScreen from './LoginScreen';
import DoneRewardScreen from './DoneRewardScreen';
import {
    LoungeScreen,
    MatchingScreen,
    QuizScreen,
    RateScreen
} from './MainScreens';
import {
    AttractionScreen,
    BodyTypeScreen,
    HeightScreen,
    NicknameScreen,
    PersonalityScreen,
    ProfileImageScreen,
    RegionScreen,
    ReligionScreen,
    SelfIntroductionScreen
} from './LegacyRegisterProfileScreens';
import {
    AuthorizationScreen,
    AuthIntroScreen,
    AuthScreen,
    SexSelectionScreen
} from './LegacySignUpScreens';
import MainScreens from './LegacyMainScreens';
import {
    ChatRoomScreen,
    RoomListScreen
} from './ChatScreens';
import RegisterProfileScreens from './RegisterProfileScreens';
import SplashScreen from './SplashScreen';
import SignUpScreens from './SignUpScreens';
import SelectPhotoScreen from './SelectPhotoScreen';

import { CustomTopBar, TopBarButton } from '../components';


export const registerScreens = (store, provider) => {
    Navigation.registerComponentWithRedux('nsome.LegacyMainScreens', () => MainScreens, provider, store);
    Navigation.registerComponentWithRedux('nsome.SplashScreen', () => SplashScreen, provider, store);

    // LaunchScreen
    Navigation.registerComponentWithRedux('nsome.LaunchScreen', () => LaunchScreen);

    // LoginScreen
    Navigation.registerComponentWithRedux('nsome.LoginScreen', () => LoginScreen, provider, store);

    // DoneRewardScreen
    Navigation.registerComponent('nsome.DoneRewardScreen', () => DoneRewardScreen);

    // RegisterProfileScreens
    Navigation.registerComponent('nsome.RegisterProfileScreens', () => RegisterProfileScreens);

    // LegacyRegisterProfileScreens
    Navigation.registerComponent('nsome.AttractionScreen', () => AttractionScreen);
    Navigation.registerComponent('nsome.BodyTypeScreen', () => BodyTypeScreen);
    Navigation.registerComponent('nsome.HeightScreen', () => HeightScreen);
    Navigation.registerComponent('nsome.NicknameScreen', () => NicknameScreen);
    Navigation.registerComponent('nsome.PersonalityScreen', () => PersonalityScreen);
    Navigation.registerComponent('nsome.ProfileImageScreen', () => ProfileImageScreen);
    Navigation.registerComponent('nsome.RegionScreen', () => RegionScreen);
    Navigation.registerComponent('nsome.ReligionScreen', () => ReligionScreen);
    Navigation.registerComponent('nsome.SelfIntroductionScreen', () => SelfIntroductionScreen);

    Navigation.registerComponent('nsome.CustomTopBar', () => CustomTopBar);
    Navigation.registerComponent('nsome.TopBarButton', () => TopBarButton);

    // SignUpScreens
    Navigation.registerComponentWithRedux('nsome.AuthIntroScreen', () => AuthIntroScreen, provider, store);
    Navigation.registerComponentWithRedux('nsome.AuthorizationScreen', () => AuthorizationScreen, provider, store);
    Navigation.registerComponentWithRedux('nsome.AuthScreen', () => AuthScreen, provider, store);
    Navigation.registerComponentWithRedux('nsome.SexSelectionScreen', () => SexSelectionScreen, provider, store);

    Navigation.registerComponent('nsome.SignUpScreens', () => SignUpScreens);

    // ChatScreens
    Navigation.registerComponentWithRedux('nsome.RoomListScreen', () => RoomListScreen, provider, store);
    Navigation.registerComponentWithRedux('nsome.ChatRoomScreen', () => ChatRoomScreen, provider, store);

    // MainScreens
    Navigation.registerComponentWithRedux('nsome.LoungeScreen', () => LoungeScreen, provider, store);
    Navigation.registerComponentWithRedux('nsome.MatchingScreen', () => MatchingScreen, provider, store);
    Navigation.registerComponentWithRedux('nsome.QuizScreen', () => QuizScreen, provider, store);
    Navigation.registerComponentWithRedux('nsome.RateScreen', () => RateScreen, provider, store);

    // SelectPhotoScreen
    Navigation.registerComponent('nsome.SelectPhotoScreen', () => SelectPhotoScreen);
};

EStyleSheet.build({
    $primary: '#00e1e4',
    $white: '#fff',
    $gray: '#c4c4c4',
});
