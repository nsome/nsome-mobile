import React, { Component } from "react"
import PropTypes from "prop-types"
import { Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import { Navigation } from "react-native-navigation"
import { CustomReactNativeCameraRollPicker } from "../../components"
import {
    ASSET_PHOTOS,
    GROUP_SAVED_PHOTOS
} from "../../constants"

class SelectPhotoScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selected: []
        }
    }

    render() {
        const { selected } = this.state;
        const { componentId } = this.props;
        const numOfEnableSelect = 6 - selected.length;
        const imgCloseButton = require("../../static/images/close_button.png");
        const imgAddPhotoAlbum = require("../../static/images/addphoto_album.png");

        return (
            <SafeAreaView style={styles.root}>
                <View style={styles.topBar}>
                    <View style={styles.buttonWrapper}>
                        <TouchableOpacity
                            onPress={() => {
                                Navigation.pop(componentId)
                            }}
                        >
                            <View>
                                <Image
                                    style={styles.imgCloseButton}
                                    source={imgCloseButton}/>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View style={styles.selectPhotoGroupWrapper}>
                                <Text
                                    style={styles.txtSelectPhotoGroup}
                                >
                                    카메라 롤
                                </Text>
                                <Image
                                    style={styles.imgSelectPhotoGroup}
                                    source={imgAddPhotoAlbum}/>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View>
                                <Text style={styles.txtButton}>등록</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                <CustomReactNativeCameraRollPicker
                    groupTypes={GROUP_SAVED_PHOTOS}
                    maximum={numOfEnableSelect}
                    selected={selected}
                    assetType={ASSET_PHOTOS}
                    emptyText={"사진이 없습니다."}
                    imagesPerRow={3}
                    imageMargin={5}
                />
            </SafeAreaView>
        )
    }
}

SelectPhotoScreen.propTypes = {
    componentId: PropTypes.string
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
        flexDirection: 'column',
    },
    topBar: {
        flex: 0,
        height: 56,
        flexDirection: 'column',
    },
    buttonWrapper: {
        flex: 5200,
        height: 52,
        marginLeft: 4,
        marginTop: 4,
        marginRight: 4,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    selectPhotoGroupWrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    txtSelectPhotoGroup: {
        color: '#505050',
        fontSize: 18,
        marginRight: 2
    },
    imgSelectPhotoGroup: {
        width: 8,
        height: 8
    },
    imgCloseButton: {
        width: 48,
        height: 48,
    },
    txtButton: {
        color: '#00e1e4',
        fontSize: 15,
        marginRight: 16
    }
});

export default SelectPhotoScreen