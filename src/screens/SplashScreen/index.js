import React, { Component } from 'react'
import PropTypes from 'prop-types';
import {
    View,
    Text,
    AsyncStorage, StyleSheet
} from 'react-native'

import { goToAuth, goMain } from '../../navigation'

import { USER_KEY } from '../../config'

class SplashScreen extends Component {
    async componentDidMount() {
        try {
            const user = await AsyncStorage.getItem(USER_KEY);
            console.log('user: ', user);
            if (user) {
                goMain();
            } else {
                goToAuth();
            }
        } catch (err) {
            console.log('error: ', err);
            goToAuth();
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>Loading</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    welcome: {
        fontSize: 28
    },
});

export default SplashScreen;
