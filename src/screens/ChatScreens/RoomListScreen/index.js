import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View, FlatList, TouchableOpacity, Image } from 'react-native';
import { Navigation } from 'react-native-navigation';

class RoomListScreen extends Component {
    static options(passProps) {
        return {
            topBar: {
                drawBehind: false,
                visible: false,
                animate: false,

            }
        };
    }
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        let items = [{msg : "굿굿"}, {msg : "굿굿"},{msg : "안녕하세요"}, {msg : "하이"}, {msg : "굿굿"}, {msg : "굿굿"}, {msg : "굿굿"}];

        return (
            <View style={styles.root}>
                <View style={styles.textWrapper}>
                    <Text style={styles.title}>메시지</Text>
                    <Text style={styles.describe}>상대의 수락 대기 중 2건 </Text>
                </View>
                <FlatList
                    style={styles.listWrapper}
                    data={items}
                    renderItem={({item}) =>
                        <View style={{alignItems:'center'}}>
                            <TouchableOpacity style={styles.chatRoomWrapper}
                                onPress={() => {
                                    Navigation.push(this.props.componentId, {
                                        component: {
                                            name: 'nsome.ChatRoomScreen',
                                        },
                                    });
                                }}>
                                <Image style={styles.pfImage} source={require("../../../static/images/pf_img1.png")}/>
                                <View>
                                    <View style={{marginLeft:20, flexDirection:'row', justifyContent:'space-between', width:250}}>
                                        <Text style={styles.nickname}>초내대신</Text>
                                        <Text style={styles.time}>방금전</Text>
                                    </View>
                                    <Text style={styles.msg}>{item.msg}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    }
                />
            </View>
        );
    }
}

RoomListScreen.propTypes = {
    navigator: PropTypes.object
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#f7f7f7'
    },
    textWrapper: {
        flex: 0.25,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10,
        fontSize: 23,
        color: '#505050'
    },
    describe: {
        alignItems: 'center',
        marginTop: 2,
        fontSize: 13,
        color: '#ababab'
    },
    chatRoomWrapper: {
        flexDirection:'row',
        width: 370,
        paddingLeft: 15,
        height: 85,
        borderRadius: 5,
        alignItems:'center',
        borderStyle: 'dashed',
        shadowOffset: {
            width: 0,
            height: 6
        },
        shadowOpacity: 0.2,
        shadowColor: '#00E1E4',
        backgroundColor: 'white',
        elevation: 3,
        marginTop: 10,
        marginBottom: 5
    },
    listWrapper: {
        flexDirection: 'column',
        backgroundColor: '#f7f7f7',
        marginTop: 10,
        paddingTop: 10,
        height:360
    },
    pfImage: {
        width:60,
        height:60,
        resizeMode:'cover',
        borderRadius:100
    },
    nickname: {
        fontWeight:'bold',
        fontSize:18
    },
    msg: {
        marginLeft:20,
        color: '#ababab',
        fontSize:15

    },
    time: {
        color: '#ababab',
        fontSize:12
    }

});

export default RoomListScreen;
