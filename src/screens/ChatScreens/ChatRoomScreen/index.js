import React, {Component} from 'react';
import {StyleSheet, Text, TextInput, View, Alert, Button, FlatList, TouchableOpacity, Image} from 'react-native';
// import CardView from 'react-native-cardview';
import { connect } from 'react-redux';
import { changeText, readMessages, writeMessage } from '../../../actions';

class ChatRoomScreen extends Component {
    static options(passProps) {
        return {
            topBar: {
                drawBehind: false,
                visible: false,
                animate: false,
            }
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            text : '',
            msgData : []
        };
        this.props.readMessages();
    }

    render() {
        return (
            <View style={styles.root}>
                <FlatList
                    onContentSizeChange={() => this.flatList.scrollToEnd({animated: true})}
                    onLayout={() => this.flatList.scrollToEnd({animated: true})}
                    ref={ref => this.flatList = ref}
                    style={styles.flatList}
                    data={this.props.msgData}
                    renderItem={({item}) => {
                        if(item.id=='소영'){
                            return (
                                <View style={styles.msgWrapper}>
                                    <Text style={styles.msgTime}>{item.time}</Text>
                                    {/*<CardView cardElevation={4}*/}
                                    {/*    cardMaxElevation={4}*/}
                                    {/*    cornerRadius={15}*/}
                                    {/*    style={styles.msgContent}>*/}
                                    {/*    <Text style={styles.msgText}>{item.msg}</Text>*/}
                                    {/*</CardView>*/}
                                </View>
                            )
                        }else{
                            return (
                                <View style={styles.msgLeftWrapper}>
                                    <Image style={styles.pfImage} source={require("../../../static/images/pf_img1.png")}/>
                                    <CardView cardElevation={4}
                                        cardMaxElevation={4}
                                        cornerRadius={15}
                                        style={styles.msgLeftContent}>
                                        <Text style={styles.msgLeftText}>{item.msg}</Text>
                                    </CardView>
                                    <Text style={styles.msgTime}>{item.time}</Text>
                                </View>
                            )
                        }
                    }
                    }
                />

                <CardView cardElevation={6}
                    cardMaxElevation={6}
                    style={styles.bottomWrapper}>
                    <TouchableOpacity >
                        <Image style={styles.attachImg} source={require('../../../static/images/attach.png')} />
                    </TouchableOpacity>
                    <TextInput style={styles.textInput}
                        autoFocus={true}
                        placeholder="메세지를 입력해주세요."
                        ref={ref => this.textInput = ref}
                        autoCorrect={false}
                        underlineColorAndroid={'transparent'}
                        selectionColor={'#dedede'}
                        onChangeText={(t) => {
                            this.props.changeText(t);
                        }}
                    />
                    <TouchableOpacity onPress={()=>{
                        this.props.writeMessage({id:"소영", msg:this.props.text, time:"time"});
                        this.textInput.clear();
                    }}>
                        <Image  style={styles.sendImg} source={require('../../../static/images/send.png')} />
                    </TouchableOpacity>
                </CardView>

            </View>
        );
    }
}


const styles = StyleSheet.create({
    root: {
        flex: 1,
        flexDirection: 'column',
    },
    flatList: {
        flex:3,
        backgroundColor:'#f7f7f7'
    },
    msgWrapper: {
        flexDirection:'row',
        justifyContent:'flex-end',
        alignItems:'flex-end',
        marginBottom:5
    },
    msgLeftWrapper: {
        flexDirection:'row',
        justifyContent:'flex-start',
        alignItems:'flex-end',
        marginBottom:5
    },
    msgContent: {
        margin:10,
        backgroundColor:'#00dce8',
        alignSelf: 'flex-end',
        width:'auto',
        padding:10,
        fontSize:24,
        maxWidth:300
    },
    msgLeftContent: {
        margin:10,
        backgroundColor:'white',
        alignSelf: 'flex-end',
        width:'auto',
        padding:10,
        fontSize:24,
        maxWidth:300
    },
    msgLeftText: {
        color:'#505050'
    },
    msgText: {
        color:'white'
    },
    msgTime: {
        marginBottom:13,
        fontSize:10,
        color:'#c4c4c4'
    },
    attachImg: {
        resizeMode: 'contain',
        width:36,
        height:36,
        marginLeft:15
    },
    sendImg: {
        resizeMode: 'contain',
        width:36,
        height:36,
        marginRight:15
    },
    bottomWrapper: {
        height:65,
        flexDirection:'row',
        alignItems:'center'
    },
    textInput: {
        height:50,
        flex:1,
        marginLeft:10,
        marginRight:10
    },
    pfImage: {
        width:40,
        height:40,
        alignSelf:'flex-start',
        resizeMode:'cover',
        borderRadius:100,
        marginTop:12
    },
});

const mapStateToProps = ({chat}) => {
    const {msgData, text} = chat;
    return {msgData, text};
};

export default connect(mapStateToProps, {
    changeText, readMessages, writeMessage
})(ChatRoomScreen);
