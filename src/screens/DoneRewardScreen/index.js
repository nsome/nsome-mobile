import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View } from 'react-native';
import {
    FlatButton,
    GetCoffeeBeanView
} from '../../components';
import { horizontalScale, verticalScale } from "../../utils/Scaling";

class DoneRewardScreen extends Component {
    static options(passProps) {
        return {
            topBar: {
                drawBehind: true,
                visible: false,
                animate: false
            }
        };
    }

    constructor(props) {
        super(props);
        // Navigation.events().bindComponent(this);
        // Navigation.mergeOptions(props.componentId, {
        //     topBar: {
        //         visible: false,
        //     },
        // });
    }

    render() {
        const {
            title,
            describe,
            buttonText,
            goTo
        } = this.props;

        return (
            <View style={styles.root}>
                <GetCoffeeBeanView title={title}/>
                <View style={styles.textWrapper}>
                    <Text style={styles.describe}>
                        매칭에 사용할 수 있는
                        <Text style={styles.emphasis}> 커피콩 10개</Text>가
                        지급되었습니다.
                    </Text>
                    <Text style={styles.describe}>
                        {describe}
                    </Text>
                    <Text style={styles.describeButton}>
                        커피콩은 무엇인가요?
                    </Text>
                </View>
                <View style={styles.buttonWrapper}>
                    <FlatButton
                        active
                        backgroundColor={'#00e1e4'}
                        color={'#fff'}
                        radius
                        title={buttonText}
                        goTo={goTo}
                    />
                </View>
            </View>
        );
    }
}

DoneRewardScreen.propTypes = {
    componentId: PropTypes.string,
    title: PropTypes.string,
    describe: PropTypes.string,
    buttonText: PropTypes.string,
    goTo: PropTypes.object,
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
        flexDirection: 'column'
    },
    body: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: horizontalScale(25),
        marginRight: horizontalScale(25),
    },
    textWrapper: {
        marginTop: verticalScale(25),
        flex: 0.15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    describe: {
        marginTop: verticalScale(2),
        fontSize: horizontalScale(13),
        color: '#505050'
    },
    describeButton: {
        marginTop: verticalScale(28),
        fontSize: horizontalScale(13),
        color: '#C4C4C4',
        textDecorationLine: 'underline'
    },
    emphasis: {
        color: '#00E1E4',
        fontWeight: 'bold',
    },
    buttonWrapper: {
        flex: 0.25,
        marginTop: verticalScale(25),
        marginLeft: horizontalScale(25),
        marginRight: horizontalScale(25),
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default DoneRewardScreen;
