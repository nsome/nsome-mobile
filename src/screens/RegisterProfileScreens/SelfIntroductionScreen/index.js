import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, TextInput, View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { FixedBottomButton } from '../../../components';

class SelfIntroductionScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selfIntroduction: ''
        };
    }

    isComplete = () => {
        return this.state.selfIntroduction.length > 0;
    };

    render() {
        return (
            <View style={styles.root}>
                <View style={styles.textWrapper}>
                    <Text style={styles.title}>자기소개</Text>
                    <Text style={styles.describe}>
                        자신에 대해 자세하게 소개해주세요.
                    </Text>
                    <Text style={styles.describe}>
                        정성스럽게 작성할수록
                        <Text style={styles.emphasis}> 매칭 성공률 UP </Text>
                    </Text>
                </View>
                <View style={styles.textInputWrapper}>
                    <TextInput
                        style={styles.textInput}
                        onChangeText={selfIntroduction =>
                            this.setState({ selfIntroduction })
                        }
                        value={this.state.selfIntroduction}
                        autoFocus={true}
                        editable={true}
                        multiline={true}
                        placeholder="취미, 관심사, 좋아하는 영화..."
                        placeholderTextColor="#C4C4C4"
                        selectionColor={'#00E1E4'}
                    />
                </View>
                <FixedBottomButton
                    isActive={this.isComplete()}
                    componentId={this.props.componentId}
                    // TODO: pushTo 프로필 완료 화면 만들기
                    pushTo="nsome.DoneRegisterProfileScreen"
                    text="프로필 입력 완료"
                />
            </View>
        );
    }
}

SelfIntroductionScreen.propTypes = {
    navigator: PropTypes.object
};

const styles = EStyleSheet.create({
    root: {
        flex: 1,
        flexDirection: 'column',
        borderTopColor: '#00E1E4',
        borderTopWidth: 2
    },
    textWrapper: {
        flex: 0.25,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textInputWrapper: {
        flex: 0.5,
        paddingLeft: '10%',
        paddingRight: '10%'
    },
    textInput: {
        paddingTop: '5%',
        fontSize: 15,
        borderTopWidth: 1,
        borderColor: '#E4E4E4'
    },
    title: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10,
        fontSize: 26,
        color: '#505050'
    },
    describe: {
        alignItems: 'center',
        marginTop: 2,
        fontSize: 13,
        color: '#505050'
    },
    emphasis: {
        color: '#00E1E4',
        fontWeight: 'bold'
    }
});

export default SelfIntroductionScreen;
