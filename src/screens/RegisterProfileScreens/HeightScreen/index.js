import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, TextInput, View } from 'react-native';
import { FixedBottomButton } from '../../../components';

class HeightScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            height: ''
        };
    }

    isComplete = () => {
        return this.state.height.length > 2;
    };

    render() {
        return (
            <View style={styles.rootContainer}>
                <View style={styles.textWrapper}>
                    <Text style={styles.title}>키</Text>
                    <Text style={styles.describe}>키가 어떻게 되세요?</Text>
                </View>
                <View style={styles.textInputWrapper}>
                    <TextInput
                        style={styles.textInput}
                        onChangeText={height => this.setState({ height })}
                        value={this.state.height}
                        autoFocus={true}
                        editable={true}
                        maxLength={3}
                        keyboardType="number-pad"
                        selectionColor={'#00E1E4'}
                    />
                    <Text style={styles.heightUnit}>cm</Text>
                </View>
                <FixedBottomButton
                    isActive={this.isComplete()}
                    componentId={this.props.componentId}
                    pushTo="nsome.BodyTypeScreen"
                    text="다음"
                />
            </View>
        );
    }
}

HeightScreen.propTypes = {
    navigator: PropTypes.object,
    componentId: PropTypes.string,
};

const styles = StyleSheet.create({
    rootContainer: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'white',
    },
    textWrapper: {
        flex: 0.25,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textInputWrapper: {
        flex: 0.2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textInput: {
        fontSize: 60,
        color: '#505050'
    },
    heightUnit: {
        marginTop: 10,
        fontSize: 15,
        color: '#C4C4C4'
    },
    title: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10,
        fontSize: 26,
        color: '#505050'
    },
    describe: {
        alignItems: 'center',
        marginTop: 2,
        fontSize: 13,
        color: '#505050'
    }
});

export default HeightScreen;
