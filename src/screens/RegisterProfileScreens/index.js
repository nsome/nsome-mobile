import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Animated,
    Dimensions,
    SafeAreaView,
    StyleSheet,
    View
} from 'react-native';

import { CustomTopBar } from "../../components";
import { BACK_BUTTON, CLOSE_BUTTON } from "../../constants";
import { pushScreen } from "../../navigation";

import AttractionScreen from "./AttractionScreen";
import BodyTypeScreen from "./BodyTypeScreen";
import HeightScreen from "./HeightScreen";
import NicknameScreen from "./NicknameScreen";
import PersonalityScreen from "./PersonalityScreen";
import ProfileImageScreen from "./ProfileImageScreen";
import RegionScreen from "./RegionScreen";
import ReligionScreen from "./ReligionScreen";
import SelfIntroductionScreen from "./SelfIntroductionScreen";
import { verticalScale } from "../../utils/Scaling";


class RegisterProfileScreens extends Component {
    static SCREEN_SIZE = 8;

    static options(passProps) {
        return {
            topBar: {
                visible: false,
                drawBehind: true,
                elevation: 0,
                noBorder: true,
            }
        }
    }

    constructor(props) {
        super(props);

        let screenWidth = Dimensions.get('window').width,
            screenHeight = Dimensions.get('window').height - CustomTopBar.TOP_BAR_HEIGHT;

        this.state = {
            mainPosition: [
                styles.mainContainer,
                { width: screenWidth * RegisterProfileScreens.SCREEN_SIZE },
                { height: screenHeight },
                { marginTop: 0 },
                { marginLeft: 0 }
            ],
            paneDimensions: [
                { width: screenWidth },
                { height: screenHeight },
            ],
            stage: 0,
        }
    }

    UNSAFE_componentWillMount() {
        this.animatedLeftMargin = new Animated.Value(0)
    }

    pushNextStage = (direction) => {
        let screenWidth = Dimensions.get('window').width,
            screenHeight = Dimensions.get('window').height - CustomTopBar.TOP_BAR_HEIGHT,
            theLeftMargin;
        if (direction === 'right') {
            theLeftMargin = parseInt('-' + screenWidth);
            Animated.timing(this.animatedLeftMargin, {
                toValue: theLeftMargin,
                duration: 200
            }).start()
        }

        this.setState({
            mainPosition: [
                styles.mainContainer,
                { width: screenWidth * RegisterProfileScreens.SCREEN_SIZE },
                { height: screenHeight },
                { marginTop: 0 },
                { marginLeft: theLeftMargin },
            ],
            stage: this.state.stage + 1,
        })
    };

    popStage = () => {
        let screenWidth = Dimensions.get('window').width,
            screenHeight = Dimensions.get('window').height - CustomTopBar.TOP_BAR_HEIGHT;

        Animated.timing(this.animatedLeftMargin, {
            toValue: 0,
            duration: 150,
        }).start();

        this.setState({
            mainPosition: [
                styles.mainContainer,
                { width: screenWidth * RegisterProfileScreens.SCREEN_SIZE },
                { height: screenHeight },
                { marginTop: 0 },
                { marginLeft: 0 },
            ],
            stage: this.state.stage - 1,
        })
    };

    render() {
        const {
            mainPosition,
            paneDimensions,
            stage,
        } = this.state;
        const { componentId } = this.props;

        return (
            <View
                style={styles.rootContainer}
            >
                <CustomTopBar
                    componentId={componentId}
                    leftButton={{
                        image: BACK_BUTTON,
                        onPress: () => {
                            if (stage === 0) {
                                pushScreen(componentId, "nsome.LoginScreen");
                            } else {
                                this.popStage()
                            }
                        }
                    }}
                    rightButton={{
                        image: CLOSE_BUTTON,
                        onPress: () => {

                        }
                    }}
                    currentStage={stage}
                    totalStage={RegisterProfileScreens.SCREEN_SIZE}
                />
                <Animated.View style={[mainPosition, { marginLeft: this.animatedLeftMargin }]}>
                    <View style={paneDimensions}>
                        <NicknameScreen/>
                    </View>
                    <View style={paneDimensions}>
                        <ProfileImageScreen/>
                    </View>
                    <View style={paneDimensions}>
                        <HeightScreen/>
                    </View>
                    <View style={paneDimensions}>
                        <BodyTypeScreen/>
                    </View>
                    <View style={paneDimensions}>
                        <ReligionScreen/>
                    </View>
                    <View style={paneDimensions}>
                        <RegionScreen/>
                    </View>
                    <View style={paneDimensions}>
                        <PersonalityScreen/>
                    </View>
                    {
                        /* 학교 인증
                        <View style={paneDimensions}>

                        </View>
                        */
                        /* 직장 인증
                       <View style={paneDimensions}>

                       </View>
                       */
                    }
                    <View style={paneDimensions}>
                        <AttractionScreen/>
                    </View>
                </Animated.View>
            </View>
        )
    }
}

RegisterProfileScreens.propTypes = {
    componentId: PropTypes.string,
};

const styles = StyleSheet.create({
    rootContainer: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'white',
    },
    mainContainer: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        backgroundColor: 'white',
    }
});

export default RegisterProfileScreens;