import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { FixedBottomButton, SingleSelectItem } from '../../../components';

class BodyTypeScreen extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.rootContainer}>
                <View style={styles.textWrapper}>
                    <Text style={styles.title}>체형</Text>
                    <Text style={styles.describe}>어떤 체형이신가요?</Text>
                </View>
                <SingleSelectItem
                    data={[
                        { id: 0, value: '마름' },
                        { id: 1, value: '조금 마름' },
                        { id: 2, value: '슬림 탄탄' },
                        { id: 3, value: '보통' },
                        { id: 4, value: '글래머' },
                        { id: 5, value: '조금 통통' },
                        { id: 6, value: '통통' }
                    ]}
                />
                <FixedBottomButton
                    // TODO: isActive 조건 만들기
                    isActive={true}
                    componentId={this.props.componentId}
                    pushTo="nsome.ReligionScreen"
                    text="다음"
                />
            </View>
        );
    }
}

BodyTypeScreen.propTypes = {
    navigator: PropTypes.object
};

const styles = EStyleSheet.create({
    rootContainer: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'white',
    },
    textWrapper: {
        flex: 0.25,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10,
        fontSize: 26,
        color: '#505050'
    },
    describe: {
        alignItems: 'center',
        marginTop: 2,
        fontSize: 13,
        color: '#505050'
    }
});

export default BodyTypeScreen;
