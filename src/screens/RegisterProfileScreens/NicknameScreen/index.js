import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from 'react-native';
import {
    horizontalScale,
    moderateScale,
    verticalScale
} from "../../../utils/Scaling";
import { FlatButton } from "../../../components";


class NicknameScreen extends  Component {
    constructor(props) {
        super(props);
        this.state = {
            nickname: '',
        };
    }

    render() {
        return (
            <View style={styles.rootContainer}>
                <View style={styles.textWrapper}>
                    <Text style={styles.title}>닉네임</Text>
                    <Text style={styles.describe}>
                        앤썸에서 사용할 닉네임을 설정해주세요!
                    </Text>
                    <Text style={styles.describe}>
                        설정된 닉네임은
                        <Text style={styles.emphasis}> 추후 변경이 불가</Text>
                        해요.
                    </Text>
                </View>
                <View style={styles.textInputWrapper}>
                    <TextInput
                        style={styles.textInput}
                        onChangeText={height => this.setState({ height })}
                        value={this.state.height}
                        autoFocus={true}
                        editable={true}
                        numberOfLines={1}
                        maxLength={12}
                        textAlignVertical={"top"}
                        keyboardType="default"
                        selectionColor={'#00E1E4'}
                        placeholder={"최대12자"}
                        placeholderTextColor={'#e4e4e4'}
                    />
                </View>
                <View style={styles.confirmButtonWrapper}>
                    <TouchableOpacity style={styles.confirmButton}>
                        <Text style={styles.confirmButtonText}>
                            닉네임 중복확인
                        </Text>
                    </TouchableOpacity>
                </View>
                <FlatButton
                    color={"#c2c2c2"}
                    text={"다음"}
                />
            </View>
        );
    }
}

NicknameScreen.propTypes = {
    componentId: PropTypes.string,
};

const styles = StyleSheet.create({
    rootContainer: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'white',
    },
    textWrapper: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        color: '#505050',
        fontSize: moderateScale(26),
        marginBottom: verticalScale(14),
    },
    describe: {
        color: '#505050',
        fontSize: moderateScale(13),
        lineHeight: verticalScale(16),
    },
    emphasis: {
        color: '#00e1e4',
    },
    textInputWrapper: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: verticalScale(55),
    },
    textInput: {
        fontSize: moderateScale(40),
        color: '#505050',
        textAlign: 'center',
    },
    confirmButtonWrapper: {
        marginTop: verticalScale(88),
        alignItems: 'center',
        justifyContent: 'center',
    },
    confirmButton: {
        borderColor: '#00E1E4',
        borderRadius: moderateScale(4),
        borderWidth: moderateScale(1),
        width: moderateScale(140),
        height: moderateScale(42),
        alignItems: 'center',
        justifyContent: 'center',
    },
    confirmButtonText: {
        color: '#00E1E4',
        fontSize: moderateScale(14),
    }
});

export default NicknameScreen;