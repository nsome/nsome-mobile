import React, { Component } from 'react';
import {
    Animated,
    Image,
    Keyboard,
    KeyboardAvoidingView,
    Platform,
    SafeAreaView,
    StatusBar,
    Text,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Navigation } from 'react-native-navigation';
import { LOGO_HEIGHT, LOGO_HEIGHT_SMALL } from '../../styles';

import { goSignup, goMain } from '../../navigation';
import { FlatButton, ImageTextInput } from '../../components';

class LoginScreen extends Component {
    static options(passProps) {
        return {
            topBar: {
                visible: false,
                drawBehind: true,
                elevation: 0,
                noBorder: true,
            },
            layout: {
                backgroundColor: 'white',
                orientation: ['portrait', 'landscape']
            }
        };
    }

    constructor(props) {
        super(props);
        Navigation.events().bindComponent(this);
        this.timer = 0;
        this.state = {
            time: {
                mm: '03',
                ss: '00'
            },
            phoneNum: '',
            authCode: '',
            checked: false,
            isSentAuthCode: false,
            isAlreadySentAuthCode: false,
            remainingTime: 180,
            authCodeVerified: false,
        };
        // this.keyboardHeight = new Animated.Value(0)
        this.logoHeight = new Animated.Value(LOGO_HEIGHT);
    }

    UNSAFE_componentWillMount() {
        this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
        this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
    }

    componentWillUnmount() {
        this.keyboardWillShowSub.remove();
        this.keyboardWillHideSub.remove();
    }

    keyboardWillShow = (event) => {
        Animated.timing(this.logoHeight, {
            duration: event.duration,
            toValue: LOGO_HEIGHT_SMALL,
        }).start();
    };

    keyboardWillHide = (event) => {
        Animated.timing(this.logoHeight, {
            duration: event.duration,
            toValue: LOGO_HEIGHT,
        }).start();
    };

    secondsToTime = (secs) => {
        const divisor_for_minutes = secs % (60 * 60);
        const minutes = Math.floor(divisor_for_minutes / 60);
        const divisor_for_seconds = divisor_for_minutes % 60;
        const seconds = Math.ceil(divisor_for_seconds);

        return {
            mm: ('0' + minutes).slice(-2),
            ss: ('0' + seconds).slice(-2)
        }
    };

    startTimer = () => {
        if (this.timer === 0 && this.state.remainingTime > 0) {
            this.timer = setInterval(this.countDown, 1000);
        }
    };

    resetTimer = (callback) => {
        clearInterval(this.timer);
        this.timer = 0;
        this.setState({
            authCodeVerified: false,
            isSentAuthCode: false,
            remainingTime: 180,
            time: this.secondsToTime(180)
        }, () => {
            if (typeof callback === 'function') {
                callback();
            }
        });
    };

    countDown = () => {
        let remainingTime = this.state.remainingTime - 1;

        this.setState({
            time: this.secondsToTime(remainingTime),
            remainingTime,
        });
        if (remainingTime === 0) {
            this.resetTimer();
        }
    };

    auth = (authCode) => {
        if (authCode === '0000') {
            this.setState({ authCodeVerified: true })
        } else {
            this.setState({ authCodeVerified: false })
        }
    };

    render() {
        const loginLogo = require('../../static/images/login_logo.png');
        const phoneIcon = require('../../static/images/phone.png');
        const authIcon = require('../../static/images/auth.png');

        const authFail = require('../../static/images/auth_fail.png');
        const authSuccess = require('../../static/images/auth_success.png');

        const {
            authCode,
            authCodeVerified,
            isAlreadySentAuthCode,
            isSentAuthCode,
            phoneNum,
            time
        } = this.state;

        return (
            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                <SafeAreaView style={styles.rootContainer}>
                    <StatusBar
                        backgroundColor="#ffffff"
                        barStyle="dark-content"
                    />
                    <KeyboardAvoidingView
                        behavior={Platform.OS === "ios" ? "padding" : false}
                        enabled
                        keyboardVerticalOffset={Platform.OS === "ios" ? -64 : false}
                        style={styles.container}
                    >
                        <View style={styles.brandIconWrapper}>
                            <Animated.Image
                                style={[styles.brandIcon, { width: this.logoHeight, height: this.logoHeight }]}
                                source={loginLogo}/>
                        </View>
                        <View style={styles.formWrapper}>
                            <ImageTextInput
                                autoFocus
                                hint="휴대폰 번호"
                                icon={phoneIcon}
                                keyboardType={"number-pad"}
                                maxLength={11}
                                onChangeText={(phoneNum) => this.setState({ phoneNum })}
                                value={phoneNum}
                            >
                                <TouchableOpacity
                                    style={[styles.inputButton,
                                        isAlreadySentAuthCode ? { width: 62 } : {},
                                        phoneNum.match(/^\d{11}$/) ? { borderColor: '#00e1e4' } : { borderColor: '#c4c4c4' }]}
                                    // phoneNum.length === 11 ? { borderColor: '#00e1e4' } : { borderColor: '#c4c4c4' }]}
                                    onPress={() => {
                                        if (phoneNum.match(/^\d{11}$/)) {
                                            this.timer === 0
                                                ? this.setState({
                                                    isSentAuthCode: true,
                                                    isAlreadySentAuthCode: true,
                                                }, () => this.startTimer())
                                                : () => {
                                                    this.resetTimer(this.startTimer)
                                                }
                                            Keyboard.dismiss()
                                        }
                                    }
                                    }
                                >
                                    <Text style={[styles.inputButtonText,
                                        phoneNum.match(/^\d{11}$/) ? { color: '#00e1e4' } : { color: '#c4c4c4' }]}
                                    >
                                        {isAlreadySentAuthCode ? "재전송" : "인증번호 전송"}
                                    </Text>
                                </TouchableOpacity>
                            </ImageTextInput>
                            <ImageTextInput
                                hint="인증번호"
                                icon={authIcon}
                                keyboardType={"number-pad"}
                                maxLength={4}
                                onChangeText={(authCode) => {
                                    this.setState({ authCode }, () => {
                                        this.auth(authCode)
                                    })
                                }}
                                value={authCode}
                            >
                                {
                                    isSentAuthCode
                                        ? <View style={styles.timerWrapper}>
                                            {
                                                authCode.length === 4
                                                    ? <Image
                                                        style={styles.authIcon}
                                                        source={authCodeVerified ? authSuccess : authFail}/>
                                                    : <View/>
                                            }
                                            {
                                                authCodeVerified
                                                    ? <View/>
                                                    : <Text style={styles.timerText}>
                                                        {time.mm + ":" + time.ss}
                                                    </Text>
                                            }
                                        </View>
                                        : <Text style={{ width: 0 }}/>
                                }
                            </ImageTextInput>
                            <View style={styles.buttonWrapper}>
                                <FlatButton
                                    active={authCodeVerified}
                                    goTo={authCodeVerified ? goMain : () => {
                                    }}
                                    radius
                                    title={'로그인'}
                                />
                            </View>
                            <View style={styles.subButtonsWrapper}>
                                <TouchableOpacity
                                    onPress={() => {
                                        // console.log(this.props.componentId)
                                        goSignup(this.props.componentId)
                                    }}
                                >
                                    <Text style={styles.subButtonText}>회원가입</Text>
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    <Text style={styles.subButtonText}>휴대폰 번호가 바뀌었어요.</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                </SafeAreaView>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = EStyleSheet.create({
    rootContainer: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    buttonWrapper: {
        height: 52,
        marginTop: 22,
        justifyContent: 'center',
        alignItems: 'center',
    },
    subButtonsWrapper: {
        width: 310,
        marginTop: 30,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    subButtonText: {
        color: '#c4c4c4',
    },
    brandIconWrapper: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 42,
    },
    brandIcon: {
        position: 'absolute',
    },
    formWrapper: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
        width: 310,
        marginBottom: 42,
    },
    mainWrapper: {
        flex: 0.7,
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputButton: {
        width: 96,
        height: 34,
        borderRadius: 4,
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputButtonText: {
        fontSize: 14,
    },
    timerWrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    timerText: {
        color: '#c4c4c4',
        fontSize: 15,
    },
    authIcon: {
        width: 14,
        height: 14,
        marginRight: 11,
    }
});

export default LoginScreen;
