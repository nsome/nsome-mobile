import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Image,
    SafeAreaView,
    StyleSheet,
    Switch,
    Text,
    TouchableOpacity,
    View,
    Keyboard, TouchableWithoutFeedback
} from 'react-native';
import {
    FlatButton,
    ImageTextInput,
    CheckBoxButton
} from '../../../components';
import DatePicker from 'react-native-datepicker'
import PickerSelect from 'react-native-picker-select'
import { horizontalScale, verticalScale } from "../../../utils/Scaling";


class PhoneAuthScreen extends Component {

    constructor(props) {
        super(props);
        this.timer = 0;
        this.picker = null;
        this.state = {
            time: {
                mm: '03',
                ss: '00'
            },
            isSentAuthCode: false,
            isAlreadySentAuthCode: false,
            remainingTime: 180,
            authCodeVerified: false,
            isAuthenticated: false,
            name: '',
            birth: '',
            carrier: '통신사',
            phoneNum: '',
            authCode: '',
            isTosChecked: false,
            isFriendAvoidChecked: false,
        };
    }

    secondsToTime = (secs) => {
        const divisor_for_minutes = secs % (60 * 60)
        const minutes = Math.floor(divisor_for_minutes / 60)
        const divisor_for_seconds = divisor_for_minutes % 60
        const seconds = Math.ceil(divisor_for_seconds)

        return {
            mm: ('0' + minutes).slice(-2),
            ss: ('0' + seconds).slice(-2)
        }
    };

    startTimer = () => {
        if (this.timer === 0 && this.state.remainingTime > 0) {
            this.timer = setInterval(this.countDown, 1000);
        }
    };

    resetTimer = (callback) => {
        clearInterval(this.timer);
        this.timer = 0;
        this.setState({
            authCodeVerified: false,
            isSentAuthCode: false,
            remainingTime: 180,
            time: this.secondsToTime(180)
        }, () => {
            if (typeof callback === 'function') {
                callback();
            }
        });
    };

    countDown = () => {
        let remainingTime = this.state.remainingTime - 1;

        this.setState({
            time: this.secondsToTime(remainingTime),
            remainingTime,
        });
        if (remainingTime === 0) {
            this.resetTimer();
        }
    };

    auth = (authCode) => {
        if (authCode === '0000') {
            this.setState({ authCodeVerified: true })
        } else {
            this.setState({ authCodeVerified: false })
        }
    };

    _toggleSwitch = () => {
        this.setState({
            isFriendAvoidChecked: !this.state.isFriendAvoidChecked
        })
    };

    _checkTos = () => {
        this.setState({
            isTosChecked: !this.state.isTosChecked
        })
    };

    render() {
        const {
            authCode,
            name,
            birth,
            phoneNum,
            isSentAuthCode,
            isAlreadySentAuthCode,
            isTosChecked,
            authCodeVerified,
            time,
        } = this.state
        const { finishSignUp } = this.props;

        const nameIcon = require('../../../static/images/name.png');
        const phoneIcon = require('../../../static/images/phone.png');
        const birthIcon = require('../../../static/images/birth.png');
        const authIcon = require('../../../static/images/auth.png');

        const authFail = require('../../../static/images/auth_fail.png');
        const authSuccess = require('../../../static/images/auth_success.png');
        const selectOptionIcon = require('../../../static/images/select_option.png');

        return (
            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                <SafeAreaView style={styles.rootContainer}>
                    <Text style={styles.title}>휴대폰 가입</Text>
                    <View style={styles.body}>
                        <ImageTextInput
                            hint="이름"
                            icon={nameIcon}
                            keyboardType={"default"}
                            maxLength={30}
                            value={name}
                            onChangeText={(value) => {
                                this.setState({ name: value })
                            }}
                        />
                        <View style={{
                            flexDirection: 'row',
                            borderBottomColor: '#eeeeee',
                            borderBottomWidth: 1,
                            width: '100%',
                            height: 52,
                            marginBottom: 8,
                            alignItems: 'center',
                        }}>
                            <Image
                                style={{
                                    resizeMode: 'contain',
                                    position: 'absolute',
                                    left: 4,
                                    width: 24
                                }}
                                source={birthIcon}/>

                            <DatePicker
                                style={{ width: 200 }}
                                customStyles={{
                                    dateInput: {
                                        paddingLeft: 42,
                                        borderWidth: 0,
                                        padding: 5,
                                        alignItems: 'flex-start'
                                    },
                                    dateText: {
                                        textAlign: 'left',
                                        fontSize: 15,
                                        color: '#000'
                                    },
                                    btnTextConfirm: {
                                        color: '#00e1e4',
                                    },
                                }}
                                showIcon={false}
                                date={birth}
                                androidMode="calendar"
                                mode="date"
                                placeholder="생년월일"
                                format="YYYY-MM-DD"
                                confirmBtnText="확인"
                                cancelBtnText="취소"
                                onDateChange={(date) => {
                                    this.setState({ birth: date })
                                }}
                            />
                        </View>

                        <ImageTextInput
                            hint="휴대폰번호"
                            icon={phoneIcon}
                            keyboardType={"number-pad"}
                            maxLength={11}
                            value={phoneNum}
                            onChangeText={(value) => {
                                this.setState({ phoneNum: value })
                            }}
                            leftChild
                        >
                            <View style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                            }}>
                                <View>
                                    <PickerSelect
                                        style={pickerSelectStyles}
                                        useNativeAndroidPickerStyle={false}
                                        ref={el => {
                                            this.picker = el
                                        }}
                                        placeholder={{
                                            label: '통신사',
                                            value: null,
                                            color: '#505050',
                                        }}
                                        placeholderTextColor="#9EA0A4"
                                        items={[
                                            {
                                                label: 'SKT',
                                                value: 'skt',
                                            },
                                            {
                                                label: 'KT',
                                                value: 'kt',
                                            },
                                            {
                                                label: 'LG',
                                                value: 'lg',
                                            },
                                            {
                                                label: '알뜰폰',
                                                value: 'prune',
                                            }
                                        ]}
                                        onValueChange={(value) => {
                                            this.setState({ carrier: value })
                                        }}
                                        value={this.state.carrier}
                                    />
                                </View>
                                <TouchableOpacity
                                    style={{ width: 24, flexDirection: 'row', justifyContent: 'center', }}
                                    onPress={() => this.picker.togglePicker(true)}
                                >
                                    <Image
                                        style={{
                                            height: 24,
                                            width: 24,
                                        }}
                                        source={selectOptionIcon}
                                    />
                                </TouchableOpacity>
                            </View>
                        </ImageTextInput>
                        <View style={styles.buttonWrapper}>
                            <TouchableOpacity
                                onPress={() => {
                                    if (phoneNum.match(/^\d{11}$/)) {
                                        this.timer === 0
                                            ? this.setState({
                                                isSentAuthCode: true,
                                                isAlreadySentAuthCode: true,
                                            }, () => this.startTimer())
                                            : () => {
                                                this.resetTimer(this.startTimer)
                                            }
                                        Keyboard.dismiss()
                                    }
                                }
                                }
                            >
                                <Text style={styles.buttonText}>인증번호 전송</Text>
                            </TouchableOpacity>
                        </View>
                        <ImageTextInput
                            hint="인증번호"
                            icon={authIcon}
                            keyboardType={"number-pad"}
                            maxLength={4}
                            onChangeText={(authCode) => {
                                this.setState({ authCode }, () => {
                                    this.auth(authCode)
                                })
                            }}
                            value={authCode}
                        >
                            {
                                isSentAuthCode
                                    ? <View style={styles.timerWrapper}>
                                        {
                                            authCode.length === 4
                                                ? <Image
                                                    style={styles.authIcon}
                                                    source={authCodeVerified ? authSuccess : authFail}/>
                                                : <View/>
                                        }
                                        {
                                            authCodeVerified
                                                ? <View/>
                                                : <Text style={styles.timerText}>
                                                    {time.mm + ":" + time.ss}
                                                </Text>
                                        }
                                    </View>
                                    : <Text style={{ width: 0 }}/>
                            }
                        </ImageTextInput>
                        <View style={styles.tosWrapper}>
                            <View style={styles.descriptionWrapper}>
                                <Text style={styles.clickableDescription}>이용약관</Text>
                                <Text style={styles.description}> 및 </Text>
                                <Text style={styles.clickableDescription}>개인정보 취급방침</Text>
                                <Text style={styles.description}> 동의(필수)</Text>
                            </View>
                            <View>
                                <CheckBoxButton
                                    checked={isTosChecked}
                                    toggle={this._checkTos}
                                />
                            </View>
                        </View>
                        <View style={styles.grayBox}>
                            <View style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between'
                            }}>
                                <Text style={{ color: '#505050', fontSize: 16 }}>아는 사람 피하기</Text>
                                <Switch
                                    ios_backgroundColor={{ true: '#b3f3f3', false: '#e4e4e4' }}
                                    trackColor={{ true: '#b3f3f3', false: '#e4e4e4' }}
                                    thumbColor={"#00e1e4"}
                                    onValueChange={this._toggleSwitch}
                                    value={this.state.isFriendAvoidChecked}
                                />
                            </View>
                            <Text style={{ marginTop: 10, color: '#c4c4c4', fontSize: 13 }}>
                                내 연락처에 있는 지인들을 등록해두면 해당 회원과의 매칭을 피할 수 있습니다.
                            </Text>
                        </View>
                        <View style={styles.nextButtonWrapper}>
                            <FlatButton
                                radius
                                title={'다음'}
                                goTo={finishSignUp}
                            />
                        </View>
                    </View>
                </SafeAreaView>
            </TouchableWithoutFeedback>
        );
    }
}

PhoneAuthScreen.propTypes = {
    navigator: PropTypes.object,
    finishSignUp: PropTypes.func,
};

const styles = StyleSheet.create({
    rootContainer: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    title: {
        fontSize: 26,
        textAlign: 'center',
        color: '#505050',
        marginTop: 8,
    },
    body: {
        flex: 1,
        flexDirection: 'column',
        marginTop: 20,
        width: 330,
    },
    buttonWrapper: {
        height: 28,
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        fontSize: 15,
        color: '#00e1e4'
    },
    tosWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 21,
    },
    clickableDescription: {
        fontSize: 15,
        textAlign: 'center',
        textDecorationLine: 'underline',
        color: '#505050'
    },
    description: {
        fontSize: 15,
        color: '#505050'
    },
    descriptionWrapper: {
        flexDirection: 'row',
        textAlign: 'center',
        alignItems: 'center'
    },
    grayBox: {
        flexDirection: 'column',
        backgroundColor: '#f5f5f5',
        marginTop: 20,
        height: 100,
        padding: 16,
        borderRadius: 6,
    },
    nextButtonWrapper: {
        height: 52,
        marginTop: 22,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    timerWrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    timerText: {
        color: '#c4c4c4',
        fontSize: 15,
    },
    authIcon: {
        width: 14,
        height: 14,
        marginRight: 11,
    }
});


const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 18,
        color: '#505050',
    },
    inputAndroid: {
        fontSize: 18,
        color: '#505050',
    },
})

export default PhoneAuthScreen;