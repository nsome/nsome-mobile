import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
    Animated,
    AsyncStorage,
    Dimensions,
    SafeAreaView,
    StyleSheet,
    View
} from 'react-native'

import { CustomTopBar } from '../../components'
import { BACK_BUTTON } from '../../constants'

import PhoneAuthScreen from './PhoneAuthScreen'
import SexSelectionScreen from './SexSelectionScreen'

import { goSignupDone, pushScreen } from '../../navigation'


class SignUpScreens extends Component {
    static SCREEN_SIZE = 2;

    static options(passProps) {
        return {
            topBar: {
                visible: false,
                drawBehind: true,
                elevation: 0,
                noBorder: true
            }
        }
    }

    constructor(props) {
        super(props);

        let screenWidth = Dimensions.get('window').width,
            screenHeight = Dimensions.get('window').height;

        this.state = {
            mainPosition: [
                styles.mainContainer,
                { width: screenWidth * SignUpScreens.SCREEN_SIZE },
                { height: screenHeight },
                { marginTop: 0 },
                { marginLeft: 0 }
            ],
            paneDimensions: [
                { width: screenWidth },
                { height: screenHeight }
            ],
            stage: 0,
            sex: '',
            isAuthenticated: false,
            name: '',
            birth: '',
            phoneNum: '',
            authCode: '',
            isTosChecked: false,
            isFriendAvoidChecked: false
        }
    }

    UNSAFE_componentWillMount() {
        this.animatedLeftMargin = new Animated.Value(0)
    }

    _storeData = async (stage) => {
        try {
            await AsyncStorage.setItem('SignUp_stage', stage)

        } catch (error) {

        }
    }

    _retrieveData = async () => {
        try {
            const stage = await AsyncStorage.getItem('SignUp_stage')

            if (stage !== null) {
                const keys = ['SignUp_sex', 'SignUp_name', 'SignUp_birth', 'SignUp_phoneNum']
                await AsyncStorage.multiGet(keys, (err, stores) => {
                    this.setState({
                        sex: stores[0][1],
                        name: stores[1][1],
                        birth: stores[2][1],
                        phoneNum: stores[3][1]
                    })
                })
            }
        } catch (error) {

        }
    };

    pushNextStage = (direction) => {
        let screenWidth = Dimensions.get('window').width,
            screenHeight = Dimensions.get('window').height,
            theLeftMargin;
        if (direction === 'right') {
            theLeftMargin = parseInt('-' + screenWidth);
            Animated.timing(this.animatedLeftMargin, {
                toValue: theLeftMargin,
                duration: 200
            }).start()
        }

        this.setState({
            mainPosition: [
                styles.mainContainer,
                { width: screenWidth * SignUpScreens.SCREEN_SIZE },
                { height: screenHeight },
                { marginTop: 0 },
                { marginLeft: theLeftMargin },
            ],
            stage: this.state.stage + 1,
        })
    };

    popStage = () => {
        let screenWidth = Dimensions.get('window').width,
            screenHeight = Dimensions.get('window').height;


        Animated.timing(this.animatedLeftMargin, {
            toValue: 0,
            duration: 150
        }).start();

        this.setState({
            mainPosition: [
                styles.mainContainer,
                { width: screenWidth * SignUpScreens.SCREEN_SIZE },
                { height: screenHeight },
                { marginTop: 0 },
                { marginLeft: 0 },
            ],
            stage: this.state.stage - 1,
        })
    };

    selectSex = (sex) => {
        this.setState({
            sex
        }, () => this.pushNextStage('right'));
    };

    render() {
        const {
            mainPosition,
            paneDimensions,
            stage
        } = this.state;
        const { componentId } = this.props;

        return (
            <SafeAreaView style={styles.rootContainer}>
                <CustomTopBar
                    componentId={componentId}
                    leftButton={{
                        image: BACK_BUTTON,
                        onPress: () => {
                            if (stage === 0) {
                                pushScreen(componentId, "nsome.LoginScreen");
                            } else {
                                this.popStage()
                            }
                        }
                    }}
                    currentStage={stage}
                    totalStage={SignUpScreens.SCREEN_SIZE}
                />
                <Animated.View style={[mainPosition, { marginLeft: this.animatedLeftMargin }]}>
                    <View style={paneDimensions}>
                        <SexSelectionScreen
                            componentId={this.props.componentId}
                            selectSex={(sex) => this.selectSex(sex)} />
                    </View>
                    <View style={paneDimensions}>
                        <PhoneAuthScreen
                            componentId={this.props.componentId}
                            finishSignUp={() => goSignupDone()} />
                    </View>
                </Animated.View>
            </SafeAreaView>
        )
    }
}

SignUpScreens.propTypes = {
    componentId: PropTypes.string
};

const styles = StyleSheet.create({
    rootContainer: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'white'
    },
    mainContainer: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        backgroundColor: 'white'
    },
});

export default SignUpScreens