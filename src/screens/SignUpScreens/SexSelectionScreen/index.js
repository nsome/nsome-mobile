import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Image,
    StyleSheet,
    Text,
    TouchableHighlight,
    View
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import { horizontalScale, moderateScale, verticalScale } from "../../../utils/Scaling";

class SexSelectionScreen extends Component {
    static MALE = "M";
    static FEMALE = "F";

    constructor(props) {
        super(props);
        Navigation.events().bindComponent(this);
        this.state = {
            pressedMan: false,
            pressedGirl: false
        };
    }

    navigationButtonPressed({ buttonId }) {
        console.log(buttonId);
    }

    render() {
        const manOval = this.state.pressedMan
            ? require('../../../static/images/male_selected.png')
            : require('../../../static/images/male.png');
        const girlOval = this.state.pressedGirl
            ? require('../../../static/images/female_selected.png')
            : require('../../../static/images/female.png');
        
        const { selectSex } = this.props;

        return (
            <View style={styles.rootContainer}>
                <Text style={styles.title}>성별 선택</Text>
                <View style={styles.body}>
                    <View style={styles.navBar}>
                        <TouchableHighlight
                            underlayColor="transparent"
                            style={styles.leftContainer}
                            onPress={() => {
                                selectSex(SexSelectionScreen.MALE)
                            }}
                            onHideUnderlay={() => {
                                this.setState({ pressedMan: false });
                            }}
                            onShowUnderlay={() => {
                                this.setState({ pressedMan: true });
                            }}
                        >
                            <Image style={[styles.sexImage, {left: 0}]} source={manOval} />
                        </TouchableHighlight>
                        <TouchableHighlight
                            underlayColor="transparent"
                            style={styles.rightContainer}
                            onPress={() => {
                                selectSex(SexSelectionScreen.FEMALE)
                            }}
                            onHideUnderlay={() => {
                                this.setState({ pressedGirl: false });
                            }}
                            onShowUnderlay={() => {
                                this.setState({ pressedGirl: true });
                            }}
                        >
                            <Image style={[styles.sexImage, {right: 0}]} source={girlOval} />
                        </TouchableHighlight>
                    </View>
                    <Text style={styles.description}>
                        서비스 품질을 위해 {'\n'} 남녀 성비는 1.5배 이내로
                        제한합니다.
                    </Text>
                </View>
            </View>
        );
    }
}

SexSelectionScreen.propTypes = {
    navigator: PropTypes.object,
    selectSex: PropTypes.func,
};

const styles = StyleSheet.create({
    rootContainer: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'white',
    },
    title: {
        fontSize: horizontalScale(26),
        textAlign: 'center',
        color: '#505050',
        marginTop: verticalScale(8),
    },
    sexImage: {
        resizeMode: 'stretch',
        position: 'absolute',
        width: moderateScale(200),
        height: moderateScale(360)
    },
    description: {
        fontSize: horizontalScale(14),
        textAlign: 'center',
        color: '#505050',
    },
    body: {
        flex: 1,
        flexDirection: 'column',
        marginTop: verticalScale(37),
    },
    navBar: {
        height: 400,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: verticalScale(46),
    },
    leftContainer: {
        position: 'absolute',
        width: horizontalScale(200),
        height: verticalScale(360),
        left: 0,
    },
    rightContainer: {
        position: 'absolute',
        width: horizontalScale(200),
        height: verticalScale(360),
        right: 0,
    }
});

export default SexSelectionScreen;
