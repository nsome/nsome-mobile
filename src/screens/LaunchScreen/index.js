import React, { Component } from 'react';
import { View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

import { goLogin } from '../../navigation';

import { FlatButton } from '../../components';

class LaunchScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.mainWrapper} />
                <View style={styles.buttonWrapper}>
                    <FlatButton
                        backgroundColor={'#00e1e4'}
                        color={'#fff'}
                        radius
                        title={'회원가입'}
                    />
                    <FlatButton
                        backgroundColor={'#fff'}
                        color={'#00e1e4'}
                        goTo={goLogin}
                        radius
                        title={'로그인'}
                    />
                </View>
            </View>
        );
    }
}

const styles = EStyleSheet.create({
    buttonWrapper: {
        flex: 0.3,
        justifyContent: 'center',
        alignItems: 'center'
    },
    container: {
        flex: 1,
        flexDirection: 'column',
        borderTopColor: '#00E1E4',
    },
    mainWrapper: {
        flex: 0.7,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default LaunchScreen;
