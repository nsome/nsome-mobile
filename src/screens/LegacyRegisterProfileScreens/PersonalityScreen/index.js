import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View } from 'react-native';
import { FixedBottomButton, MultiSelectItem } from '../../../components';

class PersonalityScreen extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.root}>
                <View style={styles.textWrapper}>
                    <Text style={styles.title}>성격</Text>
                    <Text style={styles.describe}>
                        성격을 잘 표현한 단어를
                        <Text style={styles.emphasis}> 3가지 이상 </Text>
                        골라주세요.
                    </Text>
                </View>
                <MultiSelectItem
                    data={[
                        { id: 0, value: '낙천적인' },
                        { id: 1, value: '상냥한' },
                        { id: 2, value: '지적인' },
                        { id: 3, value: '유머있는' },
                        { id: 4, value: '터프한' },
                        { id: 5, value: '섹시한' },
                        { id: 6, value: '도도한' },
                        { id: 7, value: '차분한' },
                        { id: 8, value: '자유로운' },
                        { id: 9, value: '독특한' },
                        { id: 10, value: '감성적인' },
                        { id: 11, value: '귀여운' },
                        { id: 12, value: '엉뚱한' },
                        { id: 13, value: '듬직한' },
                        { id: 14, value: '사교적인' },
                        { id: 15, value: '조용한' }
                    ]}
                />
                <FixedBottomButton
                    // TODO: isActive 조건 만들기
                    isActive={true}
                    componentId={this.props.componentId}
                    pushTo="nsome.SelfIntroductionScreen"
                    text="다음"
                />
            </View>
        );
    }
}

PersonalityScreen.propTypes = {
    navigator: PropTypes.object
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
        flexDirection: 'column',
        borderTopColor: '#00E1E4',
        borderTopWidth: 2
    },
    textWrapper: {
        flex: 0.25,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10,
        fontSize: 26,
        color: '#505050'
    },
    describe: {
        alignItems: 'center',
        marginTop: 2,
        fontSize: 13,
        color: '#505050'
    },
    emphasis: {
        color: '#00E1E4',
        fontWeight: 'bold'
    }
});

export default PersonalityScreen;
