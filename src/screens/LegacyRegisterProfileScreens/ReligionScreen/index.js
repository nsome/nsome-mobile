import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View } from 'react-native';
import { FixedBottomButton, SingleSelectItem } from '../../../components';

class ReligionScreen extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.root}>
                <View style={styles.textWrapper}>
                    <Text style={styles.title}>종교</Text>
                    <Text style={styles.describe}>
                        어떤 종교를 갖고 있나요?
                    </Text>
                </View>
                <SingleSelectItem
                    data={[
                        { id: 0, value: '없음' },
                        { id: 1, value: '천주교' },
                        { id: 2, value: '기독교' },
                        { id: 3, value: '불교' },
                        { id: 4, value: '기타' }
                    ]}
                />
                <FixedBottomButton
                    // TODO: isActive 조건 만들기
                    isActive={true}
                    componentId={this.props.componentId}
                    pushTo="nsome.RegionScreen"
                    text="다음"
                />
            </View>
        );
    }
}

ReligionScreen.propTypes = {
    navigator: PropTypes.object
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
        flexDirection: 'column',
        borderTopColor: '#00E1E4',
        borderTopWidth: 2
    },
    textWrapper: {
        flex: 0.25,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10,
        fontSize: 26,
        color: '#505050'
    },
    describe: {
        alignItems: 'center',
        marginTop: 2,
        fontSize: 13,
        color: '#505050'
    }
});

export default ReligionScreen;
