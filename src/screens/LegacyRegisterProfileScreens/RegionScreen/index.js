import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View } from 'react-native';
import { FixedBottomButton, SingleSelectItem } from '../../../components';

class RegionScreen extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.root}>
                <View style={styles.textWrapper}>
                    <Text style={styles.title}>지역</Text>
                    <Text style={styles.describe}>
                        어느 지역에 살고 계세요?
                    </Text>
                </View>
                <SingleSelectItem
                    data={[
                        { id: 0, value: '서울' },
                        { id: 1, value: '경기' },
                        { id: 2, value: '인천' },
                        { id: 3, value: '부산' },
                        { id: 4, value: '대구' },
                        { id: 5, value: '대전' },
                        { id: 6, value: '광주' },
                        { id: 7, value: '제주' }
                    ]}
                />
                <FixedBottomButton
                    // TODO: isActive 조건 만들기
                    isActive={true}
                    componentId={this.props.componentId}
                    pushTo="nsome.SelfIntroductionScreen"
                    text="다음"
                />
            </View>
        );
    }
}

RegionScreen.propTypes = {
    navigator: PropTypes.object
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
        flexDirection: 'column',
        borderTopColor: '#00E1E4',
        borderTopWidth: 2
    },
    textWrapper: {
        flex: 0.25,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10,
        fontSize: 26,
        color: '#505050'
    },
    describe: {
        alignItems: 'center',
        marginTop: 2,
        fontSize: 13,
        color: '#505050'
    }
});

export default RegionScreen;
