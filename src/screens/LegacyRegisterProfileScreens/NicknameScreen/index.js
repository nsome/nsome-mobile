import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from 'react-native';
import { horizontalScale, verticalScale } from "../../../utils/Scaling";


class NicknameScreen extends  Component {
    constructor(props) {
        super(props);
        this.state = {
            nickname: '',
        };
    }

    render() {
        return (
            <View style={styles.root}>
                <View style={styles.textWrapper}>
                    <Text style={styles.title}>닉네임</Text>
                    <Text style={styles.describe}>앤썸에서 사용할 닉네임을 설정해주세요!</Text>
                    <Text style={styles.describe}>
                        설정된 닉네임은
                        <Text stlye={styles.emphasis}> 추후 변경이 불가</Text>
                        해요.
                    </Text>
                </View>
                <View style={styles.textInputWrapper}>
                    <TextInput
                        style={styles.textInput}
                        onChangeText={height => this.setState({ height })}
                        value={this.state.height}
                        autoFocus={true}
                        editable={true}
                        maxLength={12}
                        keyboardType="default"
                        selectionColor={'#00E1E4'}
                        placeholder={"최대12자"}
                        placeholderTextColor={'#e4e4e4'}
                    />
                    <TouchableOpacity
                        style={styles.confirmButtonWrapper}
                    >
                        <Text
                            style={styles.confirmButtonText}
                        >
                            닉네임 중복확인
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

NicknameScreen.propTypes = {
    componentId: PropTypes.string,
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
        flexDirection: 'column',
    },
    textWrapper: {
        flex: 0.25,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textInputWrapper: {
        flex: 0.2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textInput: {
        fontSize: 40,
        color: '#505050'
    },
    describe: {
        marginTop: 2,
        fontSize: 13,
        color: '#505050'
    },
    emphasis: {
        color: '#00E1E4',
        fontWeight: 'bold'
    },
    confirmButtonWrapper: {
        marginTop: verticalScale(88),
        borderColor: '#00E1E4',
        borderRadius: horizontalScale(4),
        borderWidth: horizontalScale(1),
    },
    confirmButtonText: {
        color: '#00E1E4',
        fontSize: horizontalScale(14),
    }
});

export default NicknameScreen;