import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View, Text } from 'react-native'
import { goSelectPhotoScreen } from "../../../navigation";
import { ProfileImageSelector, FlatButton } from '../../../components'
import { goNext } from "../../../navigation";

class RegisterProfileScreen extends Component {
    constructor(props) {
        super(props);

        this.nextScreen = "nsome.HeightScreen";
        this.state = {
            selected: []
        }
    }

    render() {
        const { componentId } = this.props;

        return (
            <View style={styles.root}>
                <View style={styles.textWrapper}>
                    <Text style={styles.title}>프로필 사진</Text>
                    <Text style={styles.describe}>
                        얼굴이 잘 보이는 정면 사진으로
                    </Text>
                    <Text style={styles.describe}>
                        <Text style={styles.emphasis}>2장 이상 </Text>
                        등록해주세요!
                    </Text>
                </View>
                <View style={styles.imageSelectorWrapper}>
                    <ProfileImageSelector
                        isEssential={true}
                        onPress={() => goSelectPhotoScreen(this.props.componentId)}/>
                    <ProfileImageSelector isEssential={true}/>
                    <ProfileImageSelector isEssential={false}/>
                    <ProfileImageSelector isEssential={false}/>
                    <ProfileImageSelector isEssential={false}/>
                    <ProfileImageSelector isEssential={false}/>
                </View>
                <View style={styles.nextButtonWrapper}>
                    <View style={styles.button}>
                        <FlatButton
                            radius
                            title={'다음'}
                            color={'#fff'}
                            goTo={() => goNext(componentId, this.nextScreen)}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

RegisterProfileScreen.propTypes = {
    componentId: PropTypes.string
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: 52,
        marginRight: 52,
    },
    textWrapper: {
        flex: 0.25,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageSelectorWrapper: {
        flex: 0.25,
        flexWrap: 'wrap',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10,
        fontSize: 26,
        color: '#505050'
    },
    describe: {
        alignItems: 'center',
        marginTop: 2,
        fontSize: 13,
        color: '#505050'
    },
    emphasis: {
        color: '#00E1E4',
        fontWeight: 'bold'
    },
    nextButtonWrapper: {
        flex: 0.15,
        width: 330,
        justifyContent: 'center',
        alignItems: 'center',
    },
    button: {
        height: 52,
        justifyContent: 'center',
        alignItems: 'center',
    }
});

export default RegisterProfileScreen;
