import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Image, ScrollView, Text, TextInput, TouchableOpacity, View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { TagButton, FixedBottomButton } from '../../../components';

const mockTags = [
    '패션 감각', '큰 눈', '배려심이 깊어요',
    '요리를 잘해요', '예의가 발라요', '보조개',
    '손재주가 좋아요', '솔직함', '워커홀릭',
    '패션 감각', '큰 눈', '배려심이 깊어요',
    '요리를 잘해요', '예의가 발라요', '보조개',
    '손재주가 좋아요', '솔직함', '워커홀릭',
];

class AttractionScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selfIntroduction: '',
            isPushedAddTagButton: false,
            willBeAddedTag: '',
        };
    }

    addTag = () => {
        // TODO: unshift는 db에 넣는 식으로 바꿀 것
        mockTags.unshift(this.state.willBeAddedTag);

        this.setState({
            isPushedAddTagButton: false,
            willBeAddedTag: '',
        })
    };

    isComplete = () => {
        return this.state.selfIntroduction.length > 0;
    };

    render() {
        const { isPushedAddTagButton, willBeAddedTag } = this.state;
        const addTagButtonImage = require('../../../static/images/addTagButton.png');

        return (
            <View style={styles.container}>
                <View style={styles.titleWrapper}>
                    <Text style={styles.title}>나의 매력은</Text>
                </View>
                <View style={styles.tagsWrapper}>
                    <ScrollView>
                        {
                            isPushedAddTagButton ?
                                <View style={styles.addTagTextInputWrapper}>
                                    <TextInput
                                        autoFocus
                                        onChangeText={(willBeAddedTag) => this.setState({ willBeAddedTag })}
                                        onSubmitEditing={this.addTag}
                                        value={willBeAddedTag}
                                        style={styles.addTagTextInput}
                                    />
                                </View> :
                                <View style={styles.addTagButtonWrapper}>
                                    <TouchableOpacity
                                        onPress={() => this.setState({ isPushedAddTagButton: true })}
                                        style={styles.addTagButton}
                                    >
                                        <Image source={addTagButtonImage} style={styles.addTagButtonImage}/>
                                        <Text style={styles.addTagButtonText}>직접 입력하기</Text>
                                    </TouchableOpacity>
                                </View>
                        }
                        <View style={styles.tagButtonsWrapper}>
                            {
                                mockTags.map((mockTag, i) => <TagButton key={i} text={mockTag} />)
                            }
                        </View>
                    </ScrollView>
                </View>

                <FixedBottomButton
                    isActive={this.isComplete()}
                    componentId={this.props.componentId}
                    // TODO: pushTo 프로필 완료 화면 만들기
                    pushTo="nsome.DoneRegisterProfileScreen"
                    text="다음"
                />
            </View>
        );
    }
}

AttractionScreen.propTypes = {
    navigator: PropTypes.object
};

const styles = EStyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        borderTopColor: '$primary',
        borderTopWidth: 2
    },
    titleWrapper: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10,
        fontSize: 26,
        color: '#505050'
    },
    tagsWrapper: {
        flex: 5,
        paddingLeft: 32,
        paddingRight: 32,
    },
    addTagTextInputWrapper: {
        width: '43%',
        height: 38,
        backgroundColor: '$white',
        borderColor: '$primary',
        borderRadius: 4,
        borderWidth: 1,
        marginBottom: 10,
    },
    addTagTextInput: {
        height: 38,
        color: '$primary',
        fontSize: 15,
        marginLeft: 13,
    },
    addTagButtonWrapper: {
        width: '43%',
        height: 38,
        alignItems: 'center',
        justifyContent:'center',
        backgroundColor: '$primary',
        borderRadius: 4,
        marginBottom: 10,
        shadowColor: '$primary',
        shadowOpacity: 0.4,
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowRadius: 8
    },
    addTagButton: {
        flexDirection: 'row',
        alignItems: 'center',
        borderColor: '#fff',
        height: 40,
        borderRadius: 5 ,
        margin: 5,
    },
    addTagButtonImage: {
        height: 12,
        width: 12,
        marginRight: 5,
        resizeMode : 'stretch',
    },
    addTagButtonText: {
        color: '$white',
        fontSize: 15,
        fontWeight: 'bold',
        lineHeight: 22,
    },
    tagButtonsWrapper: {
        width: '100%',
        flex: 4,
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        flexDirection:'row',
    },
});

export default AttractionScreen;
