import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Dimensions, ImageBackground, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Navigation } from 'react-native-navigation';

class AuthIntroScreen extends Component {
    static options(passProps) {
        return {
            topBar: {
                drawBehind: true,
                visible: false,
                animate: false
            }
        };
    }

    constructor(props) {
        super(props);
        // Navigation.events().bindComponent(this);
    }

    render() {
        return (
            <View style={styles.root}>
                <ImageBackground
                    source={require('../../../static/images/backgrounds/guarantee_bg.png')}
                    style={styles.background}
                    resizeMode={'cover'}>
                    <Text style={styles.text}>믿을 수 있는 인연</Text>
                    <Text  style={styles.text}>앤썸에서 보증합니다!</Text>
                    <ImageBackground
                        source={require('../../../static/images/guarantee_img.png')}
                        style={styles.someIcon}
                    />
                </ImageBackground>


                <View style={styles.textWrapper}>
                    <Text style={styles.describe}>
                        믿을 수 있는 썸의 시작,
                        <Text style={styles.emphasis}> 앤썸의 개런티 매칭</Text>
                    </Text>
                    <Text style={styles.describe}>
                        본인 인증단계를 완료한 회원님들은
                    </Text>
                    <Text style={styles.describe}>
                        본인인증 이성 회원을 우선적으로 매칭해드립니다.
                    </Text>
                </View>
                <View style={styles.buttonWrapper}>
                    <TouchableOpacity style={styles.button}>
                        <Text style={styles.buttonText}>본인 인증하기</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

AuthIntroScreen.propTypes = {
    componentId: PropTypes.string
};
const window = Dimensions.get('window');

const styles = StyleSheet.create({
    root: {
        flex: 1,
        flexDirection: 'column'
    },
    textWrapper: {
        marginTop: 25,
        flex: 0.15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    describe: {
        marginTop: 2,
        fontSize: 13,
        color: '#505050'
    },
    describeButton: {
        marginTop: 15,
        fontSize: 13,
        color: '#C4C4C4',
        textDecorationLine: 'underline'
    },
    emphasis: {
        color: '#00E1E4',
        fontWeight: 'bold'
    },
    buttonWrapper: {
        flex: 0.25,
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 300,
        height: 50,
        backgroundColor: '#00E1E4',
        shadowOffset: {
            width: 0,
            height: 6
        },
        shadowOpacity: 0.2,
        shadowColor: '#00E1E4'
    },
    buttonText: {
        fontSize: 18,
        color: '#ffffff',
        fontWeight: 'bold'
    },
    background: {
        flex: 1,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    someIcon: {
        width: window.height * 0.5,
        height: window.height * 0.25,
        alignItems: 'center',
        marginTop: 10
    },
    text: {
        alignItems: 'center',
        fontSize: 26,
        color: '#505050'
    }
});

export default AuthIntroScreen;
