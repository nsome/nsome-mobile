import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, Image, View, TouchableOpacity, Switch } from 'react-native';

class EtcAuthScreen extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        // var Toggle = require('react-native-material-switch');
        var warningIcon = require('../../../../static/images/warning.png');

        return (

            <ScrollView contentContainerStyle={styles.bottom}>
                <Text style={styles.explain}>
                인증심사 담당자와 채팅을 통해
                </Text>
                <Text style={styles.explain}>
                인증을 진행할 수 있습니다.
                </Text>

                <View style={styles.buttonWrapper}>
                    <TouchableOpacity style={styles.button}>
                        <Text style={styles.buttonText}>인증하기</Text>
                    </TouchableOpacity>
                </View>

                <View  style={styles.warningWrapper}>
                    <Image style={styles.warningIcon} source={warningIcon}/>
                    <Text style={{color:'#c4c4c4', fontSize:13}}>
                    카카오톡이 설치된 기기에서만 가능합니다.
                    </Text>
                </View>

                <View  style={styles.grayBox}>

                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between'
                    }}>

                        <Text style={{color:'#505050', fontSize:16}}>
                        아는 사람 피하기
                        </Text>
                        <Switch/>

                    </View>
                    <Text style={{marginTop:10, color:'#c4c4c4', fontSize:13}}>
                    내 연락처에 있는 지인들을 등록해두면 해당 회원과의 매칭을 피할 수 있습니다.
                    </Text>
                </View>
            </ScrollView>

        );
    }
}
const styles = StyleSheet.create({
    grayBox: {
        flexDirection: 'column',
        backgroundColor:'#f5f5f5',
        marginTop:30,
        marginLeft:25,
        marginRight:25,
        height:110,
        padding:20,
        marginBottom:50
    },
    bottom: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor:'#ffffff',
        paddingTop:30,
        alignItems: 'center',
        marginBottom:50
    },
    explain: {
        color:"#505050",
        fontSize: 13,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonWrapper: {
        width: '100%'
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft:25,
        marginRight:25,
        height: 50,
        marginTop:27,
        borderColor: '#00E1E4',
        borderWidth: 1,
    },
    buttonText: {
        fontSize: 18,
        color: '#00E1E4',
        fontWeight: 'bold'
    },
    warningWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop:30
    },
    warningIcon: {
        width:17,
        height:15,
        marginRight:5
    }

});

export default EtcAuthScreen;
