import React, { Component } from 'react';
import { ScrollView, TouchableHighlight, StyleSheet, Text, View, Switch } from 'react-native';
import { ImageTextInput } from '../../../../components';

class EmailAuthScreen extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        // var Toggle = require('react-native-material-switch');
        const schoolIcon = require('../../../../static/images/school.png');
        const emailIcon = require('../../../../static/images/email.png');
        const numberIcon = require('../../../../static/images/number.png');

        return (
            <ScrollView style={styles.bottom}>
                <ImageTextInput icon={schoolIcon} hint="학교/직장명" />
                <ImageTextInput icon={emailIcon} hint="학교/직장메일"  />

                <TouchableHighlight style={styles.authenticationBtn}>
                    <Text style={styles.authenticationTxt}> 이메일로 인증번호 발송 </Text>
                </TouchableHighlight>

                <ImageTextInput icon={numberIcon}  hint="인증번호" />

                <View  style={styles.grayBox}>
                    <View style={{ flexDirection: 'row', alignItems: 'center',justifyContent: 'space-between'}}>
                        <Text style={{color:'#505050', fontSize:16}}> 아는 사람 피하기 </Text>

                        <Switch/>
                    </View>
                    <Text style={{marginTop:10, color:'#c4c4c4', fontSize:13}}>
                    내 연락처에 있는 지인들을 등록해두면 해당 회원과의 매칭을 피할 수 있습니다.
                    </Text>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    grayBox: {
        flexDirection: 'column',
        backgroundColor:'#f5f5f5',
        marginTop:20,
        marginLeft:25,
        marginRight:25,
        height:110,
        padding:20,
        marginBottom:50
    },
    bottom: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor:'#ffffff',
        marginBottom:50
    },
    authenticationTxt: {
        color:"#00e1e4",
        fontSize: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    authenticationBtn: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:23
    }

});

export default EmailAuthScreen;
