import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, View, Image, Dimensions, Switch } from 'react-native';

class PictureAuthScreen extends Component {
    constructor(props) {
        super(props) ;
    }
    render() {
        const addPicture = require('../../../../static/images/pic_add.png');

        return (
            <ScrollView contentContainerStyle={styles.bottom}>
                <Text style={styles.explain}> 학생증, 졸업증명서, 명함, 사원증 등을 </Text>
                <Text style={styles.explain}> 등록하여 인증을 진행해주세요. </Text>
                <View style={styles.imageWrapper}>
                    <Image style={styles.addPicture} source={addPicture}/>
                </View>
                <View  style={styles.grayBox}>
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between'
                    }}>
                        <Text style={{color:'#505050', fontSize:16}}>아는 사람 피하기</Text>
                        <Switch/>
                    </View>
                    <Text style={{marginTop:10, color:'#c4c4c4', fontSize:13}}>
                    내 연락처에 있는 지인들을 등록해두면 해당 회원과의 매칭을 피할 수 있습니다.
                    </Text>
                </View>

            </ScrollView>

        );
    }
}

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
    bottom: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor:'#ffffff',
        alignItems: 'center',
        paddingTop: 30,
        marginBottom:50
    },
    explain: {
        color:"#505050",
        fontSize: 13,
        justifyContent: 'center',
        alignItems: 'center',
    },
    addPicture: {
        width:width - 50,
        marginTop: 20,
        resizeMode:'contain',
        height:125
    },
    grayBox: {
        flexDirection: 'column',
        backgroundColor:'#f5f5f5',
        marginTop:17,
        marginLeft:25,
        marginRight:25,
        height:110,
        padding:20,
        marginBottom:50
    },
    imageWrapper: {
        flexDirection: 'row',
        marginLeft:25,
        marginRight:25
    }
});

export default PictureAuthScreen;
