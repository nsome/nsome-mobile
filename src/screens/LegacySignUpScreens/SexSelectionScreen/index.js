import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Dimensions,
    Image,
    SafeAreaView,
    StatusBar,
    StyleSheet,
    Text,
    TouchableHighlight,
    View
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import { CustomTopBar } from '../../../components'
import { BACK_BUTTON } from '../../../constants'

class SexSelectionScreen extends Component {
    // static options(passProps) {
    //     return {
    //         topBar: {
    //             visible: false,
    //             drawBehind: true,
    //             leftButtons: [
    //                 {
    //                     id: Constants.BACK_BUTTON,
    //                     text: "hi"
    //                     icon: require("../../../static/images/back_button.png"),
    //                 }
    //             ],
    //             rightButtons: [
    //                 {
    //                     id: Constants.BACK_BUTTON,
    //                     icon: require("../../../static/images/back_button.png")
    //                 }
    //             ],
    //             title: {
    //                 text: 'My Screen'
    //             },
    //             elevation: 0,
    //             noBorder: true,
    //             background: {
    //                 component: {
    //                     name: 'nsome.CustomTopBar',
    //                     passProps: {
    //                         leftButton: {
    //                             image: Constants.BACK_BUTTON,
    //                             onPress: () => {
    //                                 console.log("clicked");
    //                                 Navigation.pop(passProps.componentId);
    //                             }
    //                         },
    //                         rightButton: { },
    //                         currentStage: 1,
    //                         totalStage: 10,
    //                     },
    //                 }
    //             }
    //         }
    //     };
    // }

    constructor(props) {
        super(props);
        Navigation.events().bindComponent(this);
        this.state = {
            pressedMan: false,
            pressedGirl: false
        };
    }

    navigationButtonPressed({ buttonId }) {
        console.log(buttonId)
    }

    render() {
        const manOval = this.state.pressedMan
            ? require('../../../static/images/male_selected.png')
            : require('../../../static/images/male.png');
        const girlOval = this.state.pressedGirl
            ? require('../../../static/images/female_selected.png')
            : require('../../../static/images/female.png');

        return (
            <SafeAreaView style={styles.root}>
                <StatusBar
                    backgroundColor="#ffffff"
                    barStyle="dark-content"
                />
                <CustomTopBar
                    componentId={this.props.componentId}
                    leftButton= {{
                        image: BACK_BUTTON,
                        onPress: () => {
                            console.log("clicked");
                            Navigation.push(this.props.componentId, {
                                component: {
                                    name: 'nsome.LoginScreen',
                                },
                            });
                        }
                    }}
                    rightButton={{}}
                    currentStage={1}
                    totalStage={10}
                />
                <Text style={styles.title}>성별 선택</Text>
                <View style={styles.body}>
                    <View style={styles.navBar}>
                        <TouchableHighlight
                            underlayColor="transparent"
                            style={styles.leftContainer}
                            onPress={() => {
                                Navigation.push(this.props.componentId, {
                                    component: {
                                        name: 'nsome.AuthScreen',
                                    },
                                });
                            }}
                            onHideUnderlay={() => {
                                this.setState({ pressedMan: false });
                            }}
                            onShowUnderlay={() => {
                                this.setState({ pressedMan: true });
                            }}
                        >
                            <Image style={styles.sexImage} source={manOval} />
                        </TouchableHighlight>

                        <TouchableHighlight
                            underlayColor="transparent"
                            style={styles.rightContainer}
                            onPress={() => {
                                Navigation.push(this.props.componentId, {
                                    component: {
                                        name: 'nsome.AuthScreen',
                                    },
                                });
                            }}
                            onHideUnderlay={() => {
                                this.setState({ pressedGirl: false });
                            }}
                            onShowUnderlay={() => {
                                this.setState({ pressedGirl: true });
                            }}
                        >
                            <Image style={styles.sexImage} source={girlOval} />
                        </TouchableHighlight>
                    </View>
                    <Text style={styles.description}>
                        서비스 품질을 위해 {'\n'} 남녀 성비는 1.5배 이내로
                        제한합니다.
                    </Text>
                </View>
            </SafeAreaView>
        );
    }
}

SexSelectionScreen.propTypes = {
    navigator: PropTypes.object
};

const { width } = Dimensions.get('window');
const styles = StyleSheet.create({
    root: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'white',
    },
    sexImage: {
        resizeMode: 'stretch',
        width: (width * 200) / 360,
        height: (((width * 200) / 360) * 1080) / 600
    },
    description: {
        fontSize: 14,
        textAlign: 'center',
        color: '#505050',
        paddingBottom: 26,
    },
    title: {
        fontSize: 26,
        textAlign: 'center',
        color: '#505050'
    },
    body: {
        flex:1,
        flexDirection: 'column',
        justifyContent: 'center'
    },
    navBar: {
        height: 400,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 46,
    },
    leftContainer: {
        position: 'absolute',
        width: (width * 200) / 360,
        height: (((width * 200) / 360) * 1080) / 600
    },
    rightContainer: {
        position: 'absolute',
        width: (width * 200) / 360,
        height: (((width * 200) / 360) * 1080) / 600,
        right: 0
    }
});

export default SexSelectionScreen;
