import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View } from 'react-native';
import {
    FixedBottomButton,
    ImageTextInput,
    CheckBoxButton
} from '../../../components';
import { connect } from 'react-redux';
import { authUserPhone } from '../../../actions';
import { BACK_BUTTON } from '../../../constants'

class AuthScreen extends Component {
    static options(passProps) {
        return {
            topBar: {
                elevation: 0,
                noBorder: true,
                backButton: {
                    visible: false
                },
                background: {
                    component: {
                        name: 'nsome.CustomTopBar',
                        passProps: {
                            leftButton: {
                                image: BACK_BUTTON,
                            },
                            rightButton: { },
                            currentStage: 1,
                            totalStage: 10,
                        },
                    }
                }
            }
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            nickname: '',
            checked: false
        };
    }
    signupUser = () => {
        const {email, password, nickname} = this.state;
        this.props.authUserPhone({email, password, nickname});
    }

    render() {
        var idIcon = require('../../../static/images/email.png');
        var pwIcon = require('../../../static/images/password.png');
        var nickIcon = require('../../../static/images/name.png');

        return (
            <View style={styles.root}>
                <View style={styles.textWrapper}>
                    <Text style={styles.title}>가입하기</Text>
                </View>
                <ImageTextInput icon={idIcon} hint="이메일" onChangeText={(email) => this.setState({email})}/>
                <ImageTextInput icon={pwIcon} hint="비밀번호 (영문, 숫자 혼합 6자리 이상)" onChangeText={(password) => this.setState({password})}/>
                <ImageTextInput icon={nickIcon} hint="닉네임 (추후 변경 불가)" onChangeText={(nickname) => this.setState({nickname})}/>
                <View style={styles.descriptionWrapper}>
                    <Text style={styles.description}> 이용약관 및 개인정보 취급방침 동의(필수) </Text>
                    <CheckBoxButton />
                </View>
                <FixedBottomButton
                // TODO: isActive 조건 만들기
                    // isActive={true}
                    // componentId={this.props.componentId}
                    // pushTo="nsome.AuthIntroScreen"
                    // text="다음"
                    onPress={this.signupUser}
                />
            </View>
        );
    }
}

AuthScreen.propTypes = {
    navigator: PropTypes.object
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'white'
        // borderTopColor: '#00E1E4',
        // borderTopWidth: 2
    },
    textWrapper: {
        flex: 0.25,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10,
        fontSize: 26,
        color: '#505050'
    },
    description: {
        fontSize: 15,
        textAlign: 'center',
        color: '#505050',
        marginRight: 30
    },
    descriptionWrapper: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 21
    }
});

const mapStateToProps = ({auth}) => {
    const {email, password, nickname} = auth;
    return {email, password, nickname};
};

export default connect(mapStateToProps, {
    authUserPhone
})(AuthScreen);
