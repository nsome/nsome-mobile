import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';
import { CardBigView } from '../../../components';
import { ScoreView } from '../../../components';

class RateScreen extends Component {
    static options(passProps) {
        return {
            topBar: {
                visible: false,
            }
        };
    }
    constructor(props) {
        super(props);
        this.state = {
            score: 0
        };
    }

    render() {
        return (
            <View style={styles.root}>
                <View style={styles.imageSelectorWrapper}>
                    <CardBigView profileImage="../../static/images/profileImageSelection.png"
                                 nickname="강주니"
                                 region="서울"
                                 age="27"
                                 job="서울대학교"/>
                </View>
                <ScoreView score={this.state.score}
                           onSelectedScore={(index)=>{
                               this.setState({
                                   score: index
                               })
                           }} />
            </View>
        );
    }
}

RateScreen.propTypes = {
    navigator: PropTypes.object
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor:'white',
    },
    imageSelectorWrapper: {
    },
    myProfileImageWrapper: {
        height: 35,
        width: 35,
    },
    title: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10,
        fontSize: 18,
        color: '#505050'
    },
    todayMatchingWrapper: {
        height: 35,
        flexDirection: 'row',
        marginLeft: 20,
        marginTop:20
    },
    heartImage: {
        width: 25,
        height: 25
    },
    todayMatchingText: {
        color: '#c4c4c4',
        fontSize: 17,
        fontWeight: 'bold'
    },
    buttonWrapper: {
        flex: 0.2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 300,
        height: 50,
        borderColor: '#00E1E4',
        borderWidth: 1,
        borderRadius: 5,
        borderStyle: 'dashed',
        shadowOffset: {
            width: 0,
            height: 6
        },
        shadowOpacity: 0.2,
        shadowColor: '#00E1E4'
    },
    buttonText: {
        fontSize: 18,
        color: '#00E1E4',
        fontWeight: 'bold'
    },


});

export default RateScreen;
