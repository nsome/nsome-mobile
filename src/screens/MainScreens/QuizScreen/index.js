import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, Image, TouchableOpacity, ScrollView } from 'react-native';

class QuizScreen extends Component {
    static options(passProps) {
        return {
            topBar: {
                visible: false,
            }
        };
    }

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ScrollView>
                <View style={styles.root}>
                    <View style={styles.top}>
                        <View style={styles.timerWrapper}>
                            <Image style={styles.timerImage} source={require("../../../static/images/timer.png")}></Image>
                            <Text style={styles.time}>00:40:10</Text>
                        </View>
                        <Text style={styles.quizTitle}>
                            Q1. 편의점에서 뭐살까?
                        </Text>

                        <Text style={styles.quizContent}>
                            푹푹찌는 더위에 홀린 듯이 들어간 편의점 빈 손으로 나가긴 아쉬운데 뭘 사서 나갈까?
                        </Text>
                    </View>
                    <View style={styles.bottom}>
                        <TouchableOpacity style={styles.button}>
                            <View style={styles.answerWrapper}>
                                <Image style={styles.answerNum} source={require("../../../static/images/A_off.png")}></Image>
                                <Text style={styles.answerContent}>혜자 도시락</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.button}>
                            <View style={styles.answerWrapper}>
                                <Image style={styles.answerNum} source={require("../../../static/images/B_off.png")}></Image>
                                <Text style={styles.answerContent}>혜자 도시락</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.button}>
                            <View style={styles.answerWrapper}>
                                <Image style={styles.answerNum} source={require("../../../static/images/C_off.png")}></Image>
                                <Text style={styles.answerContent}>혜자 도시락</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.button}>
                            <View style={styles.answerWrapper}>
                                <Image style={styles.answerNum} source={require("../../../static/images/D_off.png")}></Image>
                                <Text style={styles.answerContent}>혜자 도시락</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.button}>
                            <View style={styles.answerWrapper}>
                                <Image style={styles.answerNum} source={require("../../../static/images/E_off.png")}></Image>
                                <Text style={styles.answerContent}>혜자 도시락</Text>
                            </View>
                        </TouchableOpacity>

                    </View>


                </View>
            </ScrollView>
        );
    }
}

QuizScreen.propTypes = {
    navigator: PropTypes.object
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor:'#f7f7f7',
        paddingBottom:30,
    },
    top: {
        flex: 0.4,
        flexDirection: 'column',
        alignItems: 'center',
        borderBottomColor: '#00E1E4',
        borderBottomWidth: 2,
        paddingBottom: 35,
        backgroundColor: 'white',
    },
    timerWrapper: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 15
    },
    timerImage: {
        width: 20,
        height: 20
    },
    time: {
        fontSize:15,
        color: '#00E1E4',
        fontWeight:'bold',
        marginLeft:10
    },
    quizTitle: {
        fontSize: 24,
        marginTop: 12,
        color: '#505050',
        fontWeight:'bold',
    },
    quizContent: {
        fontSize:15,
        color: '#505050',
        textAlign:'center',
        marginLeft: 70,
        marginRight: 70,
        marginTop: 12,
    },
    bottom: {
        flexDirection: 'column',
        backgroundColor: '#f7f7f7',
        alignItems: 'center',
        paddingTop: 20
    },
    answerWrapper: {
        flexDirection: 'row',
        alignItems: 'center',

    },
    answerNum: {
        width: 30,
        height: 30
    },
    answerContent: {
        color: '#c4c4c4',
        fontSize: 18,
        marginLeft: 15
    },

    button: {
        justifyContent: 'center',
        width: 300,
        paddingLeft: 15,
        height: 50,
        borderRadius: 5,
        borderStyle: 'dashed',
        shadowOffset: {
            width: 0,
            height: 6
        },
        shadowOpacity: 0.2,
        shadowColor: '#00E1E4',
        backgroundColor: 'white',
        elevation: 2,
        marginTop: 10,
        marginBottom: 5
    },
    buttonText: {
        fontSize: 18,
        color: '#00E1E4',
        fontWeight: 'bold'
    },
});

export default QuizScreen;
