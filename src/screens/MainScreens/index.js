export { default as LoungeScreen } from './LoungeScreen';
export { default as MatchingScreen } from './MatchingScreen';
export { default as QuizScreen } from './QuizScreen';
export { default as RateScreen } from './RateScreen';
export { default as SettingScreen } from './SettingScreen';