import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, Image, ScrollView, TouchableOpacity } from 'react-native';
import { CardSmallView } from '../../../components';

class LoungeScreen extends Component {
    static options(passProps) {
        return {
            topBar: {
                visible: false,
            }
        };
    }
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ScrollView>

                <View style={styles.root}>
                    <View style={styles.bannerWrapper}>
                        <View style={styles.messageWrapper}>
                            <Text>🎉 짝짝짝 불타는 여우님과 곰푸님의 티타임이 시작되었습니다 :)</Text>
                        </View>
                    </View>

                    <View style={styles.todayMatchingWrapper}>
                        <Image style={styles.heartImage} source={require("../../../static/images/listheart.png")}></Image>
                        <Text style={styles.todayMatchingText}>나한테 관심있는 이성</Text>
                    </View>

                    <View style={styles.imageSelectorWrapper}>
                        <CardSmallView profileImage="../../static/images/profileImageSelection.png"
                            nickname="강주니"
                            region="서울"
                            age="27"
                        />

                        <CardSmallView profileImage="../../static/images/profileImageSelection.png"
                            nickname="강주니"
                            region="서울"
                            age="27"
                        />

                        <CardSmallView profileImage="../../static/images/profileImageSelection.png"
                            nickname="강주니"
                            region="서울"
                            age="27"
                        />
                    </View>

                    <View style={styles.imageSelectorWrapper}>
                        <CardSmallView profileImage="../../static/images/profileImageSelection.png"
                            nickname="강주니"
                            region="서울"
                            age="27"
                        />

                        <CardSmallView profileImage="../../static/images/profileImageSelection.png"
                            nickname="강주니"
                            region="서울"
                            age="27"
                        />

                        <CardSmallView profileImage="../../static/images/profileImageSelection.png"
                            nickname="강주니"
                            region="서울"
                            age="27"
                        />
                    </View>
                    <View style={styles.buttonWrapper}>
                        <TouchableOpacity style={styles.button}>
                            <Text style={styles.buttonText}>카드 더 보기</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.todayMatchingWrapper}>
                        <Image style={styles.heartImage} source={require("../../../static/images/listheart.png")}></Image>
                        <Text style={styles.todayMatchingText}>내가 관심있는 이성</Text>
                    </View>

                    <View style={styles.imageSelectorWrapper}>
                        <CardSmallView profileImage="../../static/images/profileImageSelection.png"
                            nickname="강주니"
                            region="서울"
                            age="27"
                        />

                        <CardSmallView profileImage="../../static/images/profileImageSelection.png"
                            nickname="강주니"
                            region="서울"
                            age="27"
                        />

                        <CardSmallView profileImage="../../static/images/profileImageSelection.png"
                            nickname="강주니"
                            region="서울"
                            age="27"
                        />
                    </View>


                </View>
            </ScrollView>
        );
    }
}

LoungeScreen.propTypes = {
    navigator: PropTypes.object
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor:'white',
    },
    bannerWrapper: {
        height: 100
    },
    messageWrapper: {
        backgroundColor: '#f7f7f7',
    },
    imageSelectorWrapper: {
        flex: 0.35,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    myProfileImageWrapper: {
        height: 35,
        width: 35,
    },
    title: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10,
        fontSize: 18,
        color: '#505050'
    },
    todayMatchingWrapper: {
        height: 35,
        flexDirection: 'row',
        marginLeft: 20,
        marginTop:20
    },
    heartImage: {
        width: 25,
        height: 25
    },
    todayMatchingText: {
        color: '#c4c4c4',
        fontSize: 17,
        fontWeight: 'bold'
    },
    buttonWrapper: {
        flex: 0.2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#ababab'
    },
    buttonText: {
        fontSize: 14,
        color: '#ababab',
        fontWeight: 'bold',
    },


});

export default LoungeScreen;
