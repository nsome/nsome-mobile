import React, { Component } from 'react';
import { Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import PropTypes from 'prop-types';
import {
    BottomTab,
} from '../../components';
import { Navigation } from 'react-native-navigation';

import MatchingScreen from './MatchingScreen';
import QuizScreen from './QuizScreen';
import LoungeScreen from './LoungeScreen';
import RateScreen from './RateScreen';

class MainScreens extends Component {
    static options(passProps) {
        return {
            topBar: {
                drawBehind: true,
                visible: false,
                animate: false
            }
        };
    }
    constructor(props) {
        super(props);
        // Navigation.events().bindComponent(this);
        this.state = {
            selectedTab: 'card',
            selectedTabName: '오늘의 매칭'
        };

        Navigation.mergeOptions(props.componentId, {
            topBar: {
                visible: false,
            },
        });
    }

    loadSelectedScreen = (tabs) => {
        if(this.state.selectedTab===tabs[0].key){
            return (<MatchingScreen/>)
        }else if(this.state.selectedTab===tabs[1].key){
            return (<QuizScreen/>)
        }else if(this.state.selectedTab===tabs[2].key){
            return (<LoungeScreen/>)
        }else if(this.state.selectedTab===tabs[3].key){
            return (<RateScreen/>)
        }else{
            return (<MatchingScreen/>)
        }
    };
    render() {
        const tabs = [
            {key:'card', name:'매칭 리스트', activeImg:require('../../static/images/tab_card_on.png'), unactiveImg:require('../../static/images/tab_card_off.png')},
            {key:'quiz', name:'오늘의 공통분모', activeImg:require('../../static/images/tab_quiz_on.png'), unactiveImg:require('../../static/images/tab_quiz_off.png')},
            {key:'lounge', name:'라운지', activeImg:require('../../static/images/tab_lounge_on.png'), unactiveImg:require('../../static/images/tab_lounge_off.png')},
            {key:'rate', name:'매력평가', activeImg:require('../../static/images/tab_rate_on.png'), unactiveImg:require('../../static/images/tab_rate_off.png')},
            {key:'setting', name:'설정', activeImg:require('../../static/images/tab_setting_on.png'), unactiveImg:require('../../static/images/tab_setting_off.png')}
        ];

        return (
            <View style={styles.root}>
                <SafeAreaView style={styles.topWrapper}>
                    <View style={styles.leftWrapper}>
                        <TouchableOpacity>
                            <Image style={styles.beanImage} source={require("../../static/images/goshop.png")} />
                        </TouchableOpacity>
                        <Text style={styles.beanCountText}>30</Text>
                    </View>
                    <Text style={styles.title}>{this.state.selectedTabName}</Text>
                    <View style={styles.rightWrapper}>
                        <TouchableOpacity
                            onPress={() => {
                                Navigation.push(this.props.componentId, {
                                    component: {
                                        name: 'nsome.RoomListScreen',
                                    },
                                });
                            }}>
                            <Image style={styles.chatImage} source={require("../../static/images/gochat.png")} />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.myProfileImageWrapper}>
                            <Image  style={styles.myProfileImage} source={require("../../static/images/pf_img1.png")} />
                        </TouchableOpacity>
                    </View>
                </SafeAreaView>
                <View style={{flex:0.9}}>
                    {this.loadSelectedScreen(tabs)}
                </View>
                <BottomTab
                    options={tabs}
                    onChange={(index) => {
                        this.setState({ selectedTab: tabs[index].key, selectedTabName: tabs[index].name });
                    }}
                />
            </View>
        );
    }
}

MainScreens.propTypes = {
    componentId: PropTypes.string
};

const styles = StyleSheet.create({
    bottom: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor:'#ffffff'
    },
    root: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor:'#f7f7f7',
    },
    textWrapper: {
        flex: 0.25,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10,
        fontSize: 18,
        color: '#505050'
    },
    topWrapper: {
        flex: 0.1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'white'
    },
    leftWrapper: {
        flex: 1,
        flexDirection: 'row',
        marginLeft: 17,
        alignItems: 'center',
    },
    rightWrapper: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginRight: 17
    },
    beanImage: {
        height: 35,
        width: 35
    },
    beanCountText: {
        fontSize: 19,
        color: '#00e1e4'
    },
    chatImage : {
        height: 35,
        width: 35,
        marginRight: 7
    },
    myProfileImage : {
        height: 35,
        width: 35,
        borderRadius: 100
    },
});

export default MainScreens;
