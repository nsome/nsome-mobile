import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, Image, TouchableOpacity, ScrollView } from 'react-native';
import { CardView, CardSmallView } from '../../../components';

class MatchingScreen extends Component {
    static options(passProps) {
        return {
            topBar: {
                visible: false,
            }
        };
    }

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ScrollView>
                <View style={styles.root}>
                    <View style={styles.todayMatchingWrapper}>
                        <Image style={styles.heartImage} source={require("../../../static/images/listheart.png")}></Image>
                        <Text style={styles.todayMatchingText}>오늘의 매칭</Text>
                    </View>

                    <View style={styles.imageSelectorWrapper}>
                        <CardView profileImage="../../static/images/profileImageSelection.png"
                            nickname="강주니"
                            region="서울"
                            age="27"
                            job="서울대학교"/>

                        <CardView profileImage="../../static/images/profileImageSelection.png"
                            nickname="강주니"
                            region="서울"
                            age="27"
                            job="서울대학교"/>

                    </View>
                    <View style={styles.imageSelectorWrapper}>
                        <CardView profileImage="../../static/images/profileImageSelection.png"
                            nickname="강주니"
                            region="서울"
                            age="27"
                            job="서울대학교"/>

                        <CardView profileImage="../../static/images/profileImageSelection.png"
                            nickname="강주니"
                            region="서울"
                            age="27"
                            job="서울대학교"/>
                    </View>

                    <View style={styles.buttonWrapper}>
                        <TouchableOpacity style={styles.button}>
                            <Text style={styles.buttonText}>+ 카드 더 받기</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.todayMatchingWrapper}>
                        <Image style={styles.heartImage} source={require("../../../static/images/listheart.png")}></Image>
                        <Text style={styles.todayMatchingText}>지난 매칭</Text>
                    </View>
                    <View style={styles.imageSelectorWrapper}>
                        <CardSmallView profileImage="../../static/images/profileImageSelection.png"
                            nickname="강주니"
                            region="서울"
                            age="27"
                        />

                        <CardSmallView profileImage="../../static/images/profileImageSelection.png"
                            nickname="강주니"
                            region="서울"
                            age="27"
                        />

                        <CardSmallView profileImage="../../static/images/profileImageSelection.png"
                            nickname="강주니"
                            region="서울"
                            age="27"
                        />
                    </View>

                </View>
            </ScrollView>
        );
    }
}

MatchingScreen.propTypes = {
    navigator: PropTypes.object
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor:'white',
    },
    imageSelectorWrapper: {
        flex: 0.35,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    myProfileImageWrapper: {
        height: 35,
        width: 35,
    },
    title: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10,
        fontSize: 18,
        color: '#505050'
    },
    todayMatchingWrapper: {
        height: 35,
        flexDirection: 'row',
        marginLeft: 20,
        marginTop:20
    },
    heartImage: {
        width: 25,
        height: 25
    },
    todayMatchingText: {
        color: '#c4c4c4',
        fontSize: 17,
        fontWeight: 'bold'
    },
    buttonWrapper: {
        flex: 0.2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 300,
        height: 50,
        borderColor: '#00E1E4',
        borderWidth: 1,
        borderRadius: 5,
        borderStyle: 'dashed',
        shadowOffset: {
            width: 0,
            height: 6
        },
        shadowOpacity: 0.2,
        shadowColor: '#00E1E4'
    },
    buttonText: {
        fontSize: 18,
        color: '#00E1E4',
        fontWeight: 'bold'
    },


});

export default MatchingScreen;
