import  * as API  from '../apis';
import {
    READ_MESSAGES,
    WRITE_MESSAGE,
    CHANGE_TEXT
} from './actiontypes';

export const readMessages = () => {
    return (dispatch) => {
        API.readMessages(READ_MESSAGES, dispatch);
    };
};

export const writeMessage = (msgInfo) => {
    return (dispatch) =>{
        API.writeMessages(WRITE_MESSAGE, dispatch, msgInfo);
    };
};

export const changeText = (text) => {
    return {
        type: CHANGE_TEXT,
        payload: text
    };
};
