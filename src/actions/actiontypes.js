export const EMAIL_CHANGED = 'email_changed';
export const PASSWORD_CHANGED = 'password_changed';
export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const LOGIN_USER = 'login_user';

export const READ_MESSAGES = 'read_message';
export const WRTIE_MESSAGE = 'write_message';
export const CHANGE_TEXT = 'change_text';
export const SIGNUP_USER = 'signup_user';
export const CHECK_USER_STATE = 'check_user_state';

export const AUTH_USER_PHONE = 'auth_user_phone';
export const CHECK_AUTH_NUMBER = 'check_auth_number';
export const CHANGE_USER_STATE = 'chage_user_state';

/*
 action의 type을 상수로 정의하여 사용하면 action type 선언과, reducer에서 action type 구분시
 실수할 가능성을 줄일 수 있다.
 */
