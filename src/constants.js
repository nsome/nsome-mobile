// Buttons
export const BACK_BUTTON = "backButton";
export const CLOSE_BUTTON = "closeButton";

// react-native-camera-roll-picker
export const GROUP_ALBUM = "Album";
export const GROUP_ALL = "All";
export const GROUP_EVENT = "Event";
export const GROUP_FACES = "Faces";
export const GROUP_LIBRARY = "Library";
export const GROUP_PHOTO_STREAM = "PhotoStream";
export const GROUP_SAVED_PHOTOS = "SavedPhotos";

export const ASSET_PHOTOS = "Photos";
export const ASSET_VIDEOS = "Videos";
export const ASSET_ALL = "All";