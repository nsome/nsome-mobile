import firebase from 'react-native-firebase';
import * as navigation from '../navigation'

export const authPhone = (actiontype, dispatch, user_info) => {
    firebase.auth().signInWithPhoneNumber(user_info.user_hp)
        .then(confirmResult => {
            dispatch({
                type: actiontype,
                payload: confirmResult
            });
            //TODO:  navigation.goCheckAuthNumber();
        })
        .catch(error => {
            console.log(actiontype+":"+error.message);
        });
}

export const checkAuthNumber = (actiontype, dispatch, data) => {
    const {confirmResult, number, user_info} = data;
    confirmResult.confirm(number)
        .then((user) => {
            console.log(actiontype+":"+user);
            updateUserProfile(actiontype, dispatch, user_info);
        })
        .catch(error =>  {
            console.log(actiontype+":"+error.message)
        });
}

export const updateUserProfile = (actiontype, dispatch, data) => {
    const {email, nickname} = data;
    firebase.database().ref('users/').push({
        email,
        nickname
    }).then(function(){
        //TODO: navigation.go~!!!
    });
}


// export const createUser = (actiontype, dispatch, data) => {
//     authPhone(actiontype, dispatch, '+821057517630');
//
//     // const {email, password, nickname} = data;
//     // firebase.auth().createUserWithEmailAndPassword(email, password).then(function(user) {
//     //     updateUserProfile(actiontype, dispatch, data);
//     // }).catch(function(error) {
//     //     console.log(error.code);
//     // });
// }





export const getUserState = (actiontype, dispatch) => {
    console.log("kk");
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            console.log(user);
            var displayName = user.displayName;
            var email = user.email;
            var emailVerified = user.emailVerified;
            var photoURL = user.photoURL;
            var isAnonymous = user.isAnonymous;
            var uid = user.uid;
            var providerData = user.providerData;
            // ...
        } else {
            // User is signed out.
            // ...
        }
    });
}
