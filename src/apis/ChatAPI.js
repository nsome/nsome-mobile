import firebase from 'react-native-firebase';

// 메시지 가져오기
export const readMessages = (actiontype, dispatch) => {
    const db = firebase.app().database();
    db.ref('msgs/').on('value', (snap) => {
        let items = [];
        snap.forEach((child) => {

            items.push({
                msg: child.val().msg,
                id: child.val().id,
                time: child.val().time,
                key: child.key
            });
        });
        dispatch({type: actiontype, payload: items});
    });
}

// 메시지 불러오기
export const writeMessages = (actiontype, dispatch, data) => {
    let id = data.id;
    let msg = data.msg;
    let time = data.time;
    firebase.database().ref('msgs/').push({
        id,
        msg,
        time
    });
}
