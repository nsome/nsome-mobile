import { Navigation } from 'react-native-navigation';


export const goChatroom = () => Navigation.setRoot({

});

export const goMain = () => Navigation.setRoot({
    root: {
        bottomTabs: {
            options: {
                bottomTabs: {
                    animate: true,
                    drawBehind: true,
                    barStyle: 'default' | 'black',
                    translucent: true,
                    hideShadow: false
                },
            },
            children: [
                {
                    component: {
                        name: 'nsome.LoungeScreen',
                        options: {
                            bottomTab: {
                                iconInsets: { top: 0, left: 0, bottom: 0, right: 0 },
                                disableIconTint: true, //set true if you want to disable the icon tinting
                                disableSelectedIconTint: true,
                                icon: require('./static/images/tab_card_on.png'),
                                iconColor: '#009432',
                                text: '매칭 리스트',
                            },
                        },
                    }
                },
                {
                    component: {
                        name: 'nsome.MatchingScreen',
                        options: {
                            bottomTab: {
                                icon: require('./static/images/tab_card_on.png'),
                                iconColor: '#009432',
                                text: '매칭 리스트',
                            },
                        },
                    }
                },
                {
                    component: {
                        name: 'nsome.QuizScreen',
                        options: {
                            bottomTab: {
                                icon: require('./static/images/tab_card_on.png'),
                                iconColor: '#009432',
                                text: '매칭 리스트',
                            },
                        },
                    }
                },
                {
                    component: {
                        name: 'nsome.RateScreen',
                        options: {
                            bottomTab: {
                                icon: require('./static/images/tab_card_on.png'),
                                iconColor: '#009432',
                                text: '매칭 리스트',
                            },
                        },
                    }
                }
            ]
        },
    }
});

export const goToAuth = () => Navigation.setRoot({
    root: {
        component: {
            name: 'nsome.AuthIntroScreen'
        }
    }
});

export const goTutorial = () => Navigation.setRoot({

});

export const goLogin = () => Navigation.setRoot({
    root: {
        stack: {
            id: 'Login',
            children: [
                {
                    component: {
                        name: 'nsome.LoginScreen'
                    },
                },
            ]
        }
    }
});

export const goSignup = (componentId) => Navigation.push(componentId, {
    component: {
        name: 'nsome.SignUpScreens',
        options: {
            topBar: {
                visible: false,
                drawBehind: true,
            },
            layout: {
                backgroundColor: 'white',
                orientation: ['portrait', 'landscape']
            }
        },
    }
});

export const goSignupDone = () => Navigation.setRoot({
    root: {
        stack: {
            id: 'SignupDone',
            children: [
                {
                    component: {
                        name: 'nsome.DoneRewardScreen',
                        options: {
                            topBar: {
                                visible: false
                            }
                        },
                        passProps: {
                            title: '1단계 가입 완료!',
                            describe: '프로필 정보까지 등록하면, 바로 매칭을 받아볼 수 있어요!',
                            buttonText: '프로필 입력하기',
                            goTo: () => { goRegisterProfile(); }
                        },
                    }
                }
            ]
        }
    }
});

export const goRegisterProfile = () => Navigation.setRoot({
    root: {
        stack: {
            id: 'RegisterProfile',
            children: [
                {
                    component: {
                        name: 'nsome.RegisterProfileScreens',
                        options: {
                            topBar: {
                                visible: false
                            }
                        },
                    }
                }
            ]
        }
    }
});

export const goSelectPhotoScreen = (componentId) => Navigation.push(componentId, {
    component: {
        name: 'nsome.SelectPhotoScreen',
        options: {
            topBar: {
                visible: false
            }
        }
    }
});

export const pushScreen = (componentId, componentName) => Navigation.push(componentId, {
    component: {
        name: componentName,
        options: {
            topBar: {
                visible: false
            }
        }
    }
});