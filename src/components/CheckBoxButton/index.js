import React, { Component } from 'react';
import { Image, TouchableHighlight, StyleSheet } from 'react-native';

class CheckBoxButton extends Component {

    render() {
        const { checked, toggle } = this.props;

        let checkedImg = checked
            ? require('../../static/images/check_on.png')
            : require('../../static/images/check_off.png');
        return (
            <TouchableHighlight
                underlayColor="transparent"
                style={styles.root}
                onPress={toggle}
            >
                <Image style={styles.image} source={checkedImg} />
            </TouchableHighlight>
        );
    }
}

const styles = StyleSheet.create({
    root: {
        width: 22,
        height: 22,
        marginLeft: 8,
        marginRight: 8,
        backgroundColor: 'transparent'
    },
    image: {
        resizeMode: 'stretch',
        width: 22,
        height: 22
    }
});

export default CheckBoxButton;
