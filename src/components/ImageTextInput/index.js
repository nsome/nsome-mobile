import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Image, View, TextInput } from 'react-native';


const ImageTextInput = ({ autoFocus, children, hint, icon, keyboardType, leftChild, maxLength, onChangeText, value }) => {
    const textInputStyles = {};
    textInputStyles['paddingRight'] = children ? children.props.style.width + 14 : 14;

    return (
        <View style={styles.root}>
            <Image style={styles.icon} source={icon} />
            {
                !!leftChild
                && <View style={{ paddingLeft: 42 }}>{children}</View>
            }
            <TextInput
                autoCorrect={false}
                autoFocus={autoFocus}
                keyboardType={keyboardType}
                numberOfLines={1}
                maxLength={maxLength}
                onChangeText={onChangeText}
                placeholder={hint}
                selectionColor={'#dedede'}
                style={[styles.textInput, textInputStyles]}
                underlineColorAndroid={'transparent'}
                value={value}
            />
            {
                !leftChild
                && <View style={styles.button}>{children}</View>
            }
        </View>
    );
};


const styles = StyleSheet.create({
    root: {
        flexDirection: 'row',
        borderBottomColor: '#eeeeee',
        borderBottomWidth: 1,
        width: '100%',
        height: 52,
        marginBottom: 8,
        alignItems: 'center',
    },
    icon: {
        resizeMode: 'contain',
        position: 'absolute',
        left: 4,
        width: 24
    },
    textInput: {
        width: '100%',
        paddingLeft: 42,
        borderBottomWidth: 0,
        fontSize: 15
    },
    button: {
        position: 'absolute',
        right: 4
    }
});

ImageTextInput.propTypes = {
    //    icon: PropTypes.source
};

export default ImageTextInput;
