import React, { Component } from 'react';
import { Text, TouchableHighlight } from 'react-native';
import PropTypes from 'prop-types';

import EStyleSheet from 'react-native-extended-stylesheet';


class TagButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isSelected: false
        };
    }

    getStyles = (isSelected) => EStyleSheet.create({
        container: {
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '$white',
            borderColor: isSelected ? '$primary' : '$gray',
            borderRadius: 4,
            borderWidth: 1,
            marginRight: 8,
            marginBottom: 8,
        },
        text: {
            paddingTop: 8,
            paddingRight: 13,
            paddingBottom: 6,
            paddingLeft: 13,
            color: isSelected ? '$primary' : '$gray',
            fontSize: 15,
            lineHeight: 22,
        }
    });

    render() {
        const { text } = this.props;
        const { isSelected } = this.state;
        let styles = this.getStyles(isSelected);

        return (
            <TouchableHighlight
                underlayColor='transparent'
                style={styles.container}
                onPress={() => this.setState({ isSelected: !isSelected })}
            >
                <Text
                    style={styles.text}
                >
                    {text}
                </Text>

            </TouchableHighlight>
        );
    }
}

TagButton.propTypes = {
    text: PropTypes.string.isRequired
};

export default TagButton;
