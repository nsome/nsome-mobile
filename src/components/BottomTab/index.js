import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Dimensions,
    Image,
    SafeAreaView,
    StyleSheet,
    TouchableHighlight,
    View
} from 'react-native';

class BottomTab extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeOption: this.props.options[0].activeImg,
        };
    }

    // justifyContent: 'center',
    // flexDirection: 'row',
    //  backgroundColor:'#d9d9d9',
    //  shadowColor: "#000000",
    //  shadowOpacity: 0.2,
    //  shadowRadius: 2,
    //  shadowOffset: {
    //    height: 1,
    //    width: 1
    //  }
    updateActiveOption = (activeOption) => {
        this.setState({activeOption});
    };

    render() {
        return (
            <SafeAreaView style={{ backgroundColor: 'white' }}>
                <View
                    style={{
                        justifyContent: 'center',
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                        marginTop: 2,
                        borderRadius: 0,
                        elevation: 10,
                        backgroundColor: '#000000'

                    }}>
                    {this.props.options.map((option, index) => (
                        <TouchableHighlight
                            underlayColor="transparent"
                            key={index}
                            style={styles.root}
                            onPress={() => {
                                this.props.onChange(index);
                                this.updateActiveOption(option.activeImg);
                            }}>
                            <Image
                                style={styles.bottomImg}
                                source={this.state.activeOption === option.activeImg ? option.activeImg : option.unactiveImg}
                            />
                        </TouchableHighlight>
                    ))}
                </View>
            </SafeAreaView>
        )
    }
}

BottomTab.propTypes = {
    options: PropTypes.array
};
const window = Dimensions.get('window');

const styles = StyleSheet.create({
    root: {
        width: 72,
        height: 60,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',

        flex: 1
    },
    selectedTab: {
        borderBottomColor: '#00e1e4',
        borderBottomWidth: 2,
        color: "#00e1e4",
        fontSize: 15

    },
    unselectedTab: {
        borderBottomColor: '#00e1e4',
        borderBottomWidth: 0,
        color: "#c4c4c4",
        fontSize: 13
    },
    bottomImg: {
        width: window.width * 0.2,
        height: 60
    }
});
export default BottomTab;
