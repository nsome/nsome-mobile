import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Dimensions,
    ImageBackground,
    StyleSheet,
    Text,
    View
} from 'react-native';
import { horizontalScale, verticalScale } from "../../utils/Scaling";

class GetCoffeeBeanView extends Component {

    render() {
        return (
            <ImageBackground
                source={require('../../static/images/backgrounds/addBeanBackground.png')}
                style={styles.background}
                resizeMode={'cover'}
            >
                <Text style={styles.text}>{this.props.title}</Text>
                <ImageBackground
                    source={require('../../static/images/step_1_addbean_img.png')}
                    style={styles.bean}
                />
            </ImageBackground>
        );
    }
}

GetCoffeeBeanView.propTypes = {
    title: PropTypes.string,
};

const window = Dimensions.get('window');

const styles = StyleSheet.create({
    background: {
        flex: 1,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    bean: {
        width: horizontalScale(190),
        height: verticalScale(190),
        alignItems: 'center'
    },
    text: {
        flex: 0.3,
        alignItems: 'center',
        marginTop: verticalScale(10),
        marginBottom: verticalScale(10),
        fontSize: horizontalScale(26),
        color: '#007486'
    },
    beanNumWrapper: {
        position: 'absolute',
        bottom: 0,
        width: horizontalScale(72),
        height: verticalScale(46),
        justifyContent: 'center',
        alignItems: 'center',
    },
    beanNum: {
        color: '#ffffff',
        width: horizontalScale(window.width * 0.05),
    }
});

export default GetCoffeeBeanView;
