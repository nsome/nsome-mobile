import React  from 'react';
import PropTypes from 'prop-types';
import { Platform, Text, TouchableOpacity, View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { horizontalScale, moderateScale, verticalScale } from "../../utils/Scaling";

const FlatButton = ({ active, backgroundColor, color, goTo, radius, skip, text }) => {

    return (
        <View style={styles.rootContainer}>
            {
                skip
                    ? <TouchableOpacity style={styles.skipButton} onPress={goTo}>
                        <Text style={styles.skipButtonText}>{"SKIP"}</Text>
                    </TouchableOpacity>
                    : null
            }
            <TouchableOpacity
                style={{
                    ...styles.mainButton,
                    backgroundColor: active ? styles.mainButton.backgroundColor : '#e4e4e4',
                }}
                onPress={goTo}>
                <Text
                    style={{
                        ...styles.mainButtonText,
                        color: active? styles.mainButtonText.color : '#c2c2c2',
                    }}>
                    {text}
                </Text>
            </TouchableOpacity>
        </View>
    )
};

FlatButton.propTypes = {
    active: PropTypes.bool,
    backgroundColor: PropTypes.string,
    color: PropTypes.string,
    goTo: PropTypes.func,
    skip: PropTypes.bool,
    text: PropTypes.string.required,
};

FlatButton.defaultProps = {
    active: false,
    backgroundColor: '#00e1e4',
    skip: false,
};

const styles = EStyleSheet.create({
    rootContainer: {
        alignSelf: 'center',
        flex: .2,
        flexDirection: 'row',
        position: 'absolute',
        bottom: Platform.OS === "ios" ? 20 : 26,
        width: horizontalScale(330),
        height: verticalScale(52),
        marginLeft: moderateScale(15),
        marginRight: moderateScale(15),
    },
    skipButton: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        marginRight: 10,
        borderRadius: moderateScale(6),
        backgroundColor: '#efefef',
    },
    skipButtonText: {
        color: '#c2c2c2',
        fontSize: moderateScale(16),
    },
    mainButton: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        borderRadius: moderateScale(6),
        backgroundColor: '#00e1e4',
    },
    mainButtonText: {
        color: '#fff',
        fontSize: moderateScale(16),
    }
});

export default FlatButton;
