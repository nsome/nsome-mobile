import React from 'react';
import PropTypes from 'prop-types';
import {
    LayoutAnimation,
    NativeModules,
    View
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

const { UIManager } = NativeModules;

UIManager.setLayoutAnimationEnabledExperimental &&
    UIManager.setLayoutAnimationEnabledExperimental(true);

let CustomLayoutAnimation = {
    duration: 200,
    create: {
        type: LayoutAnimation.Types.linear,
        property: LayoutAnimation.Properties.opacity,
    },
    update: {
        type: LayoutAnimation.Types.easeInEaseOut,
    },
};

const TopProgressBar = ({ currentStage, totalStage }) => {
    let progressRatio = currentStage / totalStage;

    return (
        <View style={styles.progressBarWrapper}>
            <View style={[styles.progress, { flex: progressRatio }]} />
            <View style={[styles.left, { flex: 1 - progressRatio }]} />
        </View>
    )
};

TopProgressBar.propTypes = {
    currentStage: PropTypes.number,
    totalStage: PropTypes.number
};

const styles = EStyleSheet.create({
    progressBarWrapper: {
        flex: 1,
        flexDirection: 'row',
        height: 2,
        marginBottom: 2,
    },
    progress: {
        height: 2,
        backgroundColor: '#00e1e4',
    },
    left: {
        height: 2,
        backgroundColor: '#fff',
    }
})

export default TopProgressBar;
