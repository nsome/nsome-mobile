import React from 'react';
import PropTypes from 'prop-types';
import { Image, StyleSheet, TouchableOpacity } from 'react-native';

const ProfileImageSelector = ({ isEssential, onPress }) => {
    const profileImageSelection = isEssential
        ? require('../../static/images/proimg_ess.png')
        : require('../../static/images/proimg.png');

    return (
        <TouchableOpacity
            onPress={onPress}
        >
            <Image
                source={profileImageSelection}
                style={styles.root}/>
        </TouchableOpacity>
    );

};

ProfileImageSelector.propTypes = {
    isEssential: PropTypes.bool,
    onPress: PropTypes.func,
};

const styles = StyleSheet.create({
    root: {
        width: 102,
        height: 102,
        marginLeft: 6,
        marginRight: 6,
        marginBottom: 12
    }
});

export default ProfileImageSelector;
