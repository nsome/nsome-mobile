import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Animated,
    Image,
    LayoutAnimation,
    NativeModules,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    SafeAreaView,
    StyleSheet,
    View
} from 'react-native';

import EStyleSheet from 'react-native-extended-stylesheet';
import { BACK_BUTTON, CLOSE_BUTTON } from '../../constants';
import { horizontalScale, moderateScale, verticalScale } from "../../utils/Scaling";

const { UIManager } = NativeModules;

UIManager.setLayoutAnimationEnabledExperimental &&
UIManager.setLayoutAnimationEnabledExperimental(true);

let CustomLayoutAnimation = {
    duration: 200,
    create: {
        type: LayoutAnimation.Types.linear,
        property: LayoutAnimation.Properties.opacity,
    },
    update: {
        type: LayoutAnimation.Types.easeInEaseOut,
    },
};

class CustomTopBar extends Component {
    static TOP_BAR_HEIGHT = verticalScale(56);

    constructor(props) {
        super(props);

        this.state = {
            currentStage: 0,
            progressBarRatio: [
                styles.progress,
                { flex: 0 }
            ],
            progressBarLeftRatio: [
                styles.left,
                { flex: 1 }
            ],
            leftButtonImage: this.setButtonImage(props.leftButton["image"]),
            rightButtonImage: this.setButtonImage(props.rightButton["image"]),
        };
    }

    UNSAFE_componentWillMount() {
        this.animatedProgress = new Animated.Value(0)
    }

    shouldComponentUpdate(nextProps, nextState) {
        const { currentStage, totalStage } = this.props;
        console.log(currentStage);
        console.log(nextProps.currentStage);

        if (nextProps.currentStage !== currentStage) {
            console.log("1");
            Animated.timing(this.animatedProgress, {
                toValue: parseInt(currentStage / totalStage) * 100,
                duration: 200,
            });

            return true;
        }
        console.log("2");

        return false;
    }

    onPress = (currentStage) => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);

        this.setState({ currentStage })
    };

    setButtonImage = (buttonTag) => {
        const backButton = require("../../static/images/back_button.png");
        const closeButton = require("../../static/images/close_button.png");

        let buttonImage;

        if (buttonTag === BACK_BUTTON) {
            buttonImage = backButton
        } else if (buttonTag === CLOSE_BUTTON) {
            buttonImage = closeButton
        } else {
            buttonImage = null
        }

        return buttonImage;
    };

    render() {
        const { leftButton, rightButton } = this.props;
        const {
            progressBarRatio,
            progressBarLeftRatio,
            leftButtonImage,
            rightButtonImage
        } = this.state;
        console.log(this.props);

        return (
            <View style={styles.rootContainer}>
                {
                    <View style={styles.progressBarWrapper}>
                        <View style={[progressBarRatio, { flex: this.animatedProgress }]}/>
                        <View style={[progressBarLeftRatio, { flex: 1 - this.animatedProgress }]}/>
                    </View>
                }
                <View style={styles.buttonWrapper}>
                    {
                        Object.keys(leftButton).length !== 0
                        && !!leftButtonImage
                        && <TouchableOpacity onPress={() => {
                            leftButton.onPress()
                        }}>
                            <View>
                                <Image style={styles.button} source={leftButtonImage}/>
                            </View>
                        </TouchableOpacity>
                    }
                    {
                        Object.keys(rightButton).length !== 0
                        && !!rightButtonImage
                        && <TouchableOpacity>
                            <View>
                                <Image style={styles.button} source={rightButtonImage}/>
                            </View>
                        </TouchableOpacity>
                    }
                </View>
            </View>
        )
    }
}

CustomTopBar.propTypes = {
    leftButton: PropTypes.object,
    rightButton: PropTypes.object,
};

CustomTopBar.defaultProps = {
    leftButton: {},
    rightButton: {},
};

const styles = EStyleSheet.create({
    rootContainer: {
        flex: 0,
        height: CustomTopBar.TOP_BAR_HEIGHT,
        flexDirection: 'column',
    },
    buttonWrapper: {
        flex: 5200,
        height: verticalScale(52),
        marginLeft: horizontalScale(4),
        marginTop: horizontalScale(4),
        marginRight: horizontalScale(4),
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    button: {
        width: moderateScale(48),
        height: moderateScale(48),
    },
    progressBarWrapper: {
        flex: 1,
        flexDirection: 'row',
        height: verticalScale(2),
        marginBottom: verticalScale(2),
    },
    progress: {
        height: verticalScale(2),
        backgroundColor: '#00e1e4'
    },
    left: {
        height: verticalScale(2),
        backgroundColor: '#fff'
    },
});

export default CustomTopBar;
