import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, TouchableOpacity, View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

class SingleSelectItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedItem: null
        };
    }

    onPressItem = item => {
        this.setState({
            selectedItem: item
        });
    };

    isActiveItem = itemKey => {
        const selectedItem = this.state.selectedItem;

        if (selectedItem) {
            return selectedItem.id === itemKey ? '#00E1E4' : '#C4C4C4';
        } else {
            return '#C4C4C4';
        }
    };

    render() {
        const { data } = this.props;

        return (
            <View style={styles.selectItemWrapper}>
                {data.map((item, index) => {
                    return (
                        <TouchableOpacity
                            style={EStyleSheet.child(
                                styles,
                                'itemWrapper',
                                index,
                                data.length
                            )}
                            key={index}
                            onPress={() => this.onPressItem(item)}
                        >
                            <View>
                                <Text
                                    style={[
                                        styles.itemText,
                                        { color: this.isActiveItem(index) }
                                    ]}
                                >
                                    {item.value}
                                </Text>
                            </View>
                        </TouchableOpacity>
                    );
                })}
            </View>
        );
    }
}

SingleSelectItem.propTypes = {
    data: PropTypes.array
};

const styles = EStyleSheet.create({
    selectItemWrapper: {
        flex: 0.5,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
    },
    itemWrapper: {
        borderColor: '#E4E4E4',
        width: '80%',
        height: '15%',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomWidth: 1
    },
    'itemWrapper:first-child': {
        borderTopWidth: 1
    },
    itemText: {
        fontSize: 15,
        color: '#C4C4C4'
    }
});

export default SingleSelectItem;
