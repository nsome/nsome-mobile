import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Image, StyleSheet, View, TouchableOpacity } from 'react-native';

class ScoreView extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const offImage = require("../../static/images/rate_off.png");
        const onImage = require("../../static/images/rate_on.png");

        return (

            <View style={styles.root}>
                <TouchableOpacity style={styles.imageWrapper}
                    onPress={() => this.props.onSelectedScore(1)} >
                    <Image source={this.props.score>=1?onImage:offImage} style={styles.starImage} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.imageWrapper}
                    onPress={() => this.props.onSelectedScore(2)} >
                    <Image source={this.props.score>=2?onImage:offImage} style={styles.starImage} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.imageWrapper}
                    onPress={() => this.props.onSelectedScore(3)} >
                    <Image source={this.props.score>=3?onImage:offImage} style={styles.starImage} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.imageWrapper}
                    onPress={() => this.props.onSelectedScore(4)} >
                    <Image source={this.props.score>=4?onImage:offImage} style={styles.starImage} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.imageWrapper}
                    onPress={() => this.props.onSelectedScore(5)} >
                    <Image source={this.props.score>=5?onImage:offImage} style={styles.starImage} />
                </TouchableOpacity>

            </View>


        )
    }
}

ScoreView.propTypes = {
    score: PropTypes.number,
    onSelectedScore: PropTypes.func
};

const styles = StyleSheet.create({
    root: {
        flex:1,
        flexDirection:'row',
        justifyContent:'center'
    },
    imageWrapper: {
        height:60,
        marginRight:7,
        width: 60,
    },
    starImage: {
        height:60,
        width: 60,
    }
});

export default ScoreView;
