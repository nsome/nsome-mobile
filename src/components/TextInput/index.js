import React from 'react';
import PropTypes from 'prop-types';
import { Image, StyleSheet, TextInput, TouchableOpacity, View } from 'react-native';

const TextInput = ({ buttonTitle, hint, icon, onChangeText, onPressButton }) => {
    return (
        <View style={styles.root}>
            <View style={styles.imageWrapper}>
	            <Image style={styles.icon} source={icon}/>
            </View>
            <View style={styles.inputWrapper}>
                <TextInput
                    style={styles.textInput}
                    onChangeText={onChangeText}
                    autoFocus={true}
                    autoCorrect={false}
                    underlineColorAndroid={'transparent'}
                    selectionColor={'#dedede'}
                    placeholder={hint}
                />
            </View>
            <View style={styles.buttonWrapper}>
                <TouchableOpacity
                    onPress={onPressButton}
                    style={ this.props.style }
                >
                    <Text>{buttonTitle}</Text>
                </TouchableOpacity>
            </View>
	      </View>
    );
};

ImageTextInput.propTypes = {
//    icon: PropTypes.source
};

const styles = StyleSheet.create({
    root: {
        marginLeft: 25,
        marginRight: 25,
        borderBottomColor: '#dedede',
        borderBottomWidth: 1,
        flexDirection: 'row',
        height: 52,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop:10
    },
    imageWrapper: {

    },
    inputWrapper: {

    },
    buttonWrapper: {

    },
    icon: {
        resizeMode: 'contain',
        width: 24,
        marginLeft: 8.5,
        marginRight: 14
    },
    textInput: {
        width: 250,
        borderBottomWidth: 0,
        fontSize: 15
    }
});

export default ImageTextInput;
