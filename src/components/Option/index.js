import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, TouchableHighlight, StyleSheet, View } from 'react-native';

class Option extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeOption: this.props.options[0]
        };
    }

    updateActiveOption = activeOption => {
        this.setState({
            activeOption
        });
    };

    render() {

        return (
            <View
                style={{
                    justifyContent: 'center',
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    marginTop: 10
                }}
            >
                {this.props.options.map((option, index) => (
                    <TouchableHighlight
                        key={index}
                        underlayColor='transparent'
                        style={styles.root}
                        onPress={() => {
                            this.props.onChange(index);
                            this.updateActiveOption(option);
                        }}>
                        <Text
                            style={this.state.activeOption === option ?styles.selectedTab:styles.unselectedTab}
                        >
                            {option}
                        </Text>
                    </TouchableHighlight>
                ))}
            </View>
        );
    }
}

Option.propTypes = {
    options: PropTypes.array,
    onChange: PropTypes.func
};

const styles = StyleSheet.create({
    root: {
        width: 68,
        height: 25,
        justifyContent: 'center',
        alignItems: 'center'
    },
    selectedTab : {
        borderBottomColor: '#00e1e4',
        borderBottomWidth: 2,
        color:"#00e1e4",
        fontSize: 15

    },
    unselectedTab : {
        borderBottomColor: '#00e1e4',
        borderBottomWidth: 0,
        color:"#c4c4c4",
        fontSize: 13
    }
});

export default Option;
