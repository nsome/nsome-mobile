import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Image, StyleSheet, View, Text, TouchableOpacity } from 'react-native';

class CardView extends Component {
    render() {
        const profileImage = require("../../static/images/pf_img1.png");
        const athenticationImage = require("../../static/images/guarantee_16.png");

        return (

            <View style={styles.root}>
                <TouchableOpacity style={styles.imageWrapper}>
                    <Image source={profileImage} style={styles.profileImage} />
                </TouchableOpacity>
                <View style={styles.nicknameWrapper}>
                    <Text style={styles.nickname}>
                        {this.props.nickname}
                    </Text>
                    <Image source={athenticationImage} style={styles.athenticationImage} />
                </View>
                <View>
                    <Text style={styles.userInfoWrapper}>
                        {this.props.region} | {this.props.age} | {this.props.job}
                    </Text>
                </View>
            </View>


        )
    }
}

CardView.propTypes = {
    profileImage: PropTypes.string,
    nickname: PropTypes.string,
    region: PropTypes.string,
    age: PropTypes.string,
    job: PropTypes.string,
};

const styles = StyleSheet.create({
    root: {
        width: 156,
        height: 212,
        marginLeft: 13,
        marginRight: 13,
        marginBottom:20
    },
    imageWrapper: {
        height:156,
        width: 156,

    },
    profileImage: {
        height:156,
        width:156,
        borderRadius:10
    },
    athenticationImage: {
        height:20,
        width:20,
        marginLeft:5
    },
    nicknameWrapper: {
        flex: 1,
        flexDirection: 'row',
        marginTop:14
    },
    nickname: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    userInfoWrapper: {
        color: '#ababab'

    }
});

export default CardView;
