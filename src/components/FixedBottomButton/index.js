import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { Navigation } from 'react-native-navigation';

const FixedBottomButton = ({ isActive, text, pushTo, componentId, onPress }) => {
    return (
        <TouchableOpacity
            style={[
                styles.root,
                isActive ? styles.rootActive : styles.rootInactive
            ]}
            // onPress={() => {
            //     if (isActive) {
            //         Navigation.push(componentId, {
            //             component: {
            //                 name: pushTo,
            //             },
            //         });
            //     }
            // }}
            onPress={onPress}
        >
            <Text
                style={[
                    styles.text,
                    isActive ? styles.textActive : styles.textInactive
                ]}
            >
                {text}
            </Text>
        </TouchableOpacity>
    );
};

FixedBottomButton.propTypes = {
    isActive: PropTypes.bool,
    text: PropTypes.string,
    pushTo: PropTypes.string,
    navigator: PropTypes.object,
    componentId: PropTypes.string,
    options: PropTypes.object,
};

const styles = StyleSheet.create({
    root: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 0
    },
    rootInactive: {
        backgroundColor: '#E4E4E4'
    },
    rootActive: {
        backgroundColor: '#00E1E4'
    },
    text: {
        fontSize: 18
    },
    textInactive: {
        color: '#C2C2C2'
    },
    textActive: {
        color: '#FFFFFF'
    }
});

export default FixedBottomButton;
