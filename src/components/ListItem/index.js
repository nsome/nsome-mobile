import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Text, View } from 'react-native';

class ListItem extends PureComponent {
    _onPress = () => {
        this.props.onPressItem(this.props.id);
    };
    render() {
        const textColor = this.props.selected ? '#00E1E4' : '#C4C4C4';
        return (
            <TouchableOpacity onPress={this._onPress}>
                <View>
                    <Text style={{ color: textColor }}>
                        {this.props.content}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }
}

ListItem.propTypes = {
    onPressItem: PropTypes.func,
    id: PropTypes.string,
    selected: PropTypes.boolean,
    content: PropTypes.string,
};

export default ListItem;
