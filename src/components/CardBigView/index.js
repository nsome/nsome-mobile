import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Image, StyleSheet, View, Text, TouchableOpacity } from 'react-native';

class CardBigView extends Component {
    render() {
        const profileImage = require("../../static/images/pf_img1.png");
        const athenticationImage = require("../../static/images/guarantee_16.png");

        return (

            <View style={styles.root}>
                <TouchableOpacity style={styles.imageWrapper}>
                    <Image source={profileImage} style={styles.profileImage} />
                </TouchableOpacity>

                <View style={styles.userInfo}>
                    <View style={styles.nicknameWrapper}>
                        <Text style={styles.nickname}>
                            {this.props.nickname}
                        </Text>
                        <Image source={athenticationImage} style={styles.athenticationImage} />
                    </View>
                    <View>
                        <Text style={styles.userInfoWrapper}>
                            {this.props.region} | {this.props.age}
                        </Text>
                        <Text style={styles.userInfoWrapper}>
                            {this.props.job}
                        </Text>
                    </View>
                </View>
            </View>


        )
    }
}

CardBigView.propTypes = {
    profileImage: PropTypes.string,
    nickname: PropTypes.string,
    region: PropTypes.string,
    age: PropTypes.string,
    job: PropTypes.string,
};

const styles = StyleSheet.create({
    root: {
        width: 380,
        height: 370,
        marginTop: 50,
        marginLeft: 13,
        marginRight: 13,
        marginBottom:20,
    },
    imageWrapper: {
        width: '100%',
        height: 370,
        flex:1,
        flexDirection:'row',
        justifyContent:'center',
    },
    profileImage: {
        height:360,
        width:360,
        borderRadius:10,
    },
    athenticationImage: {
        height:20,
        width:20,
        marginLeft:5
    },
    userInfo: {
        paddingTop:38,
        paddingLeft:35,
        paddingBottom:25,
        position: 'absolute',
        bottom: 0,
    },
    nicknameWrapper: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop:14,
        marginBottom:7
    },
    nickname: {
        fontSize: 20,
        color:'white',
        fontWeight: 'bold'

    },
    userInfoWrapper: {
        color:'white',
    }
});

export default CardBigView;
