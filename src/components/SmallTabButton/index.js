import React, { Component } from 'react';
import { Text, Image, TouchableHighlight, StyleSheet, View } from 'react-native';

class SmallTabButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: false
        };
    }

    render() {
        return (
            <TouchableHighlight
                underlayColor='transparent'
                style={styles.root}
                onPress={() => this.setState({selected: !this.state.selected})}
            >
                <Text
                    style={(this.state.selected ? styles.selectedTab : styles.unselectedTab)}
                >
                    {this.props.text}
                </Text>

            </TouchableHighlight>
        );
    }
}

const styles = StyleSheet.create({
    root: {
        width: 68,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center' 
    },
    selectedTab : {
    	borderBottomColor: '#00e1e4',
        borderBottomWidth: 2,
        color:"#00e1e4",
        fontSize: 13
        
    },
    unselectedTab : {
    	borderBottomColor: '#00e1e4',
    	borderBottomWidth: 0,
        color:"#c4c4c4",
        fontSize: 13
    }
});

export default SmallTabButton;
