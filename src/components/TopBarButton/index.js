import React from 'react';
import PropTypes from 'prop-types';
import { Image, TouchableHighlight, View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

const TopBarButton = ({ image, onPress }) => {
    return (
        <TouchableHighlight onPress={() => onPress()} underlayColor={'transparent'}>
            <View>
                <Image style={styles.button} source={image} />
            </View>
        </TouchableHighlight>
    )
};

TopBarButton.propTypes = {
    image: PropTypes.number,
    onPress: PropTypes.func
};

const styles = EStyleSheet.create({
    button: {
        width: 48,
        height: 48,
    },
})

export default TopBarButton;
