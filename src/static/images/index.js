import React, { Component } from 'react';
import {StyleSheet, Text, View } from 'react-native';
import PropTypes from 'prop-types';
import {
    FixedBottomButton,
    Option,
} from '../../../components';
import EmailAuthScreen from './EmailAuthScreen';
import PictureAuthScreen from './PictureAuthScreen';
import EtcAuthScreen from './EtcAuthScreen';

class AuthenticationScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedTab: '메일인증',
        };
    }

    loadSelectedScreen = (tabs) => {

        if(this.state.selectedTab===tabs[0]){
            return (<EmailAuthScreen/>)
        }else if(this.state.selectedTab===tabs[1]){
            return (<PictureAuthScreen/>)
        }else{
            return (<EtcAuthScreen/>)
        }
    };

    render() {
        let tabs = ['메일인증', '사진인증', '기타인증'];

        return (
            <View style={styles.root}>
                <View style={styles.textWrapper}>
                    <Text style={styles.title}>본인인증</Text>
                    <Text style={styles.describe}>여성회원의 본인 인증은 선택사항이나{'\n'} 매력있는 남성회원과의 매칭을 위해 입력을 권장합니다 :)</Text>
                </View>

                <Option
                    options={tabs}
                    onChange={(index) => {
                        this.setState({ selectedTab: tabs[index] });
                    }}
                />
                {this.loadSelectedScreen(tabs)}

                <FixedBottomButton
                    // TODO: isActive 조건 만들기
                    isActive={true}
                    componentId={this.props.componentId}
                    pushTo="nsome.SexSelectionScreen"
                    text="다음"
                />
            </View>
        );
    }
}
AuthenticationScreen.propTypes = {
    navigator: PropTypes.object,
    componentId: PropTypes.string
};


const styles = StyleSheet.create({
    bottom: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor:'#ffffff'
    },
    root: {
        flex: 1,
        flexDirection: 'column',
        // borderTopColor: '#00E1E4',
        // borderTopWidth: 2,
        backgroundColor:'#f5f5f5'
    },
    textWrapper: {
        flex: 0.25,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10,
        fontSize: 26,
        color: '#505050'
    },
    describe: {
        alignItems: 'center',
        marginTop: 2,
        textAlign:'center',
        fontSize: 13,
        color: '#505050'
    },
    authenticationTxt: {
        color:"#00e1e4",
        fontSize: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    authenticationBtn: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:23
    }
});

export default AuthenticationScreen;
