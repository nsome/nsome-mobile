import { Dimensions, PixelRatio } from 'react-native';

const { width, height } = Dimensions.get('window');
const guidelineBaseWidth = 360;
const guidelineBaseHeight = 640;

const horizontalScale = size => PixelRatio.roundToNearestPixel(width / guidelineBaseWidth * size);
const verticalScale = size => PixelRatio.roundToNearestPixel(height / guidelineBaseHeight * size);
const moderateScale = (size, factor = 0.5) => size + ( verticalScale(size) - size ) * factor;

export {
    horizontalScale,
    moderateScale,
    verticalScale
};