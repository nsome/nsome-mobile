import {
    READ_MESSAGES,
    CHANGE_TEXT
} from '../actions/actiontypes';

const INITIAL_STATE = {
    text : '',
    msgData : []
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case READ_MESSAGES:
            return {...state, msgData: action.payload};
        case CHANGE_TEXT:
            return {...state, text: action.payload};
        default:
            return state;
    }
};
