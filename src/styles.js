import { Dimensions } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'

const window = Dimensions.get('window')

export const LOGO_HEIGHT = 110
export const LOGO_HEIGHT_SMALL = window.width / 7

export default EStyleSheet.create({

})
