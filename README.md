# nsome mobile

CRNA(Create React Native App)을 eject하고, ExpoKit 옵션을 사용하여 프로젝트 폴더를 만들었습니다.

## install

### Expo XDE

ExpoKit을 사용해서 개발할 수 있게 [Expo XDE](https://docs.expo.io/versions/latest/introduction/installation)를 설치합니다.

### Android Studio

android 시뮬레이터 설치 및 실행을 쉽게 하기 위해 [Android Studio](https://developer.android.com/studio/)를 설치합니다.

### Xcode

ios 빌드와 시뮬레이터 실행을 위해 [Xcode](https://developer.apple.com/xcode/) 설치합니다.

### CocoaPods

Xcode의 라이브러리들을 사용하기 위해, 의존성 관리 프로그램인 [CocoaPods](https://cocoapods.org/)를 설치합니다.

## local 개발 환경 세팅

프로젝트 경로 최상단에서 node modules를 설치합니다.

```console
yarn install
```

ios 폴더에 들어가 라이브러리를 설치합니다.

```console
cd ios
pod install
```