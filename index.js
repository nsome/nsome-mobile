import { Navigation } from 'react-native-navigation';
import { registerScreens } from './src/screens';
import reducers from './src/reducers';
import {createStore, applyMiddleware} from 'redux';
import ReduxThunk from 'redux-thunk';
import {Provider} from 'react-redux';
import { goLogin, goRegisterProfile } from './src/navigation'

let store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
registerScreens(store, Provider);

Navigation.events().registerAppLaunchedListener(() => goRegisterProfile());
